<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'xdNGduA]yd;Y+?SvuC[z4|]p23A^Ke5{2,^<M`-]M!6.KHJsr9:kT-a{!C@]R0%#' );
define( 'SECURE_AUTH_KEY',  'BK@6*0r6_h!hQ<jR~)WBq<IpUxyi9;O?yZa) wFa/(Z)G6^`aPC}]bvYcVb_mS%a' );
define( 'LOGGED_IN_KEY',    ':i;<!}jzK)#%mE?N]=DVlS1,bhSA*~EmI<j?xHisk]4w:!%]oXR59Y,&tL{A_e$:' );
define( 'NONCE_KEY',        'i?K)qG%VZZteJm*ZGY},F-kOxi[9*KM9f3*zac^S!z%{(954t8]2)c3$Qqh[NiKy' );
define( 'AUTH_SALT',        'aq9yWxgV?F|O>$k{(FZ%x(d3.<EQuw[]ujJ<4=bt*=:Q1z{ ouhAMOSOVUb[Td%.' );
define( 'SECURE_AUTH_SALT', '(LL ]](!#YgQB/G._.AxFm%5{/z(+Ks+0 lan.[!bo&Y%iAH#565l?7ooU}[A>iV' );
define( 'LOGGED_IN_SALT',   'OGl]6_Qdy^]XSv4@G.2l3~_u&/MRh9lvW~DJ0)+TJxL]lNT*4~eeV]w?Vs:]eaS,' );
define( 'NONCE_SALT',       '*Uw|A|HP%L:3.ca@k0*_5!u9<XhsD)zl$PJO{jQ~4R]hL4<D;-jLSn7nx<9cVWSn' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
define( 'WP_CACHE', false ); //RabbitLoader
require_once ABSPATH . 'wp-settings.php';
