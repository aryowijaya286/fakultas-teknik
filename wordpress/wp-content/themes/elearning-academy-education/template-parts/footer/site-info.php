<?php
/**
 * Displays footer site info
 *
 * @package Elearning Academy Education
 * @subpackage elearning_academy_education
 */

?>
<div class="site-info">
	<div class="container">
		<p><?php echo esc_html(get_theme_mod('elearning_education_footer_text',__('Elearning Academy Education WordPress Theme By ThemesPride','elearning-academy-education'))); ?></p>
	</div>
</div>