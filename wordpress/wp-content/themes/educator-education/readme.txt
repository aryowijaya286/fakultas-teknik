=== Educator Education WordPress Theme ===
Contributors: Themeshopy
Tags: wide-blocks, flexible-header, block-styles, block-patterns, left-sidebar, right-sidebar, one-column, two-columns, grid-layout, custom-colors, custom-background, custom-logo, custom-menu, custom-header, editor-style, featured-images, footer-widgets, full-width-template, theme-options, threaded-comments, translation-ready, rtl-language-support, e-commerce, education, portfolio
Requires at least: 5.0
Tested up to: 6.1
Requires PHP: 7.2
Stable tag: 0.4.3
License: GPLv3.0 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Educator Education is a clean, simple, and professionally developed WordPress theme for universities, kindergartens, schools, academies, educational institutions, teachers, tutors, learning management systems, training and coaching centers, language schools, and e-learning portals.

== Description ==

Educator Education is a clean, simple, and professionally developed WordPress theme for universities, kindergartens, schools, lms based website, language school, business school, arts schools, academies, educational institutions, teachers, tutors, learning management systems, training and coaching centers, language schools, and e-learning portals. Its adaptable and highly responsive layout is going to make the design work exceptionally well on various screens and devices. The user-friendly interface will make it easy for anyone irrespective of their coding skills, to create a functional website. It has got a flexible design that comes with some personalization options for making a few changes. With SEO-friendly HTML codes included in the design, there is hardly anything you would have to do for getting better SEO ranks as simply using a proper keyword would serve the purpose. You have the secure and clean codes that are going to reflect in your website’s performance as well since your web page is going to have a faster page load time. It is possible to achieve a lot with the various social media icons available with the theme. With a Bootstrap framework being used, this theme is such a robust WordPress theme that can handle high traffic flows as well. There are many Call to Action Button (CTA) included along with plenty of social media icons and animation effects.

== Changelog ==

= 0.1 =
* Initial version Released.

= 0.2 =
* Removed post formats from theme.
* Added RTL CSS file.
* Added woocommerce my account folder.

= 0.3 =
* Added blog post border radius option.
* Added blog post box shadow option.
* Added about theme option in dashboard.
* Added footer link option.
* Added POT file.

= 0.3.1 =
* Added top bottom slider content spacing settings.
* Updated POT file.

= 0.3.2 =
* Added left right slider content spacing settings.
* Updated POT file.

= 0.3.3 =
* Added post time settings.
* Updated POT file.

= 0.3.4 =
* Added slider responsive setting for mobile.
* Updated POT file.

= 0.3.5 =
* Added condition for slider image in custom-front-page.php file.
* Added scroll to top responsive setting for mobile.
* Updated POT file.

= 0.3.6 =
* Changed sidebar and blog post layout.

= 0.3.7 =
* Added block pattern in the theme.

= 0.3.8 =
* Added menu color option.
* Added menu hover color option.
* Added sub-menu color option.
* Updated .pot file.

= 0.3.9 =
* Added toggle button color option.
* Updated .pot file.

= 0.4 =
* Added logo padding option.
* Added logo margin option.
* Added site title font color option.
* Added site tagline font color option.
* Added show/hide single post comment box option.
* Added POT file.

= 0.4.1 =
* Added sub-menu hover color option.
* Added product sale top, bottom, left and right padding option.
* Added preloader type option.
* Added menu font weight option.
* Added show / hide single post breadcrumb option.
* Added show / hide single page breadcrumb option.
* Updated .pot file.

= 0.4.2 =
* Added prefixing for variables for post formats.
* Added requires at least in style.css.
* Removed esc_url in function.php.
* Changed font family function.
* Added the webfont license
* Updated pot file.

= 0.4.3 =
* Resolve css error for Copyright text alignment.
* Added shop page sidebar layout option.
* Added single product page sidebar layout option.
* Updated pot file.

== Resources ==

Educator Education WordPress Theme, Copyright 2022 Themeshopy
Educator Education is distributed under the terms of the GNU GPL

Educator Education WordPress Theme is derived from Twenty Sixteen WordPress Theme, Copyright 2014-2015 WordPress.org
Twenty Sixteen is distributed under the terms of the GNU GPL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see .

Theme is Built using the following resource bundles.

1. Bootstrap 
	- Mark Otto
	- copyright 2011-2021, Mark Otto
	- https://github.com/twbs/bootstrap/releases/download/v5.0.0/bootstrap-5.0.0-dist.zip
	- License: Code released under the MIT License.
	- https://github.com/twbs/bootstrap/blob/main/LICENSE

2. Font-Awesome 
	- Davegandy
	- copyright July 12, 2018, Davegandy
	- https://github.com/FortAwesome/Font-Awesome.git
	- License: Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License
	- https://github.com/FortAwesome/Font-Awesome/blob/master/LICENSE.txt

3. Customizer Pro 
	- Justin Tadlock
	- Copyright 2016, Justin Tadlock
	- https://github.com/justintadlock/trt-customizer-pro.git
	- License: GNU General Public License v2.0
	- http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

4. Superfish 
	- Joeldbirch
	- Copyright 2013, Justin Tadlock
	- https://github.com/joeldbirch/superfish.git
	- License: Free to use and abuse under the MIT license. v1.7.9
	- https://github.com/joeldbirch/superfish/blob/master/MIT-LICENSE.txt
	
5. Webfonts Loader
	- https://github.com/WPTT/webfont-loader
	- License: https://github.com/WPTT/webfont-loader/blob/master/LICENS

6. Owl Carousel
	- David Deutsch
	- Copyright 2013-2017 David Deutsch
	- https://github.com/OwlCarousel2/OwlCarousel2
	- License: The MIT License (MIT)
	- https://github.com/OwlCarousel2/OwlCarousel2/blob/develop/LICENSE

7. Screenshot Images
	License: CC0 1.0 Universal (CC0 1.0) 
	Source: https://pxhere.com/en/license

	License: CC0 1.0 Universal (CC0 1.0)
	Source: https://pxhere.com/en/photo/867306

	License: CC0 1.0 Universal (CC0 1.0)
	Source: https://pxhere.com/en/photo/1246944

	License: CC0 1.0 Universal (CC0 1.0)
	Source: https://pxhere.com/en/photo/1591995

	License: CC0 1.0 Universal (CC0 1.0)
	Source: https://pxhere.com/en/photo/1224306