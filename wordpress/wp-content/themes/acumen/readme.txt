=== Acumen ===
Contributors: ffthemes
Tags: blog, news, entertainment, grid-layout, one-column, two-columns, right-sidebar, custom-background, custom-header, custom-logo, custom-menu, editor-style, featured-image-header, featured-images, flexible-header, footer-widgets, full-width-template, rtl-language-support, sticky-post, theme-options, threaded-comments, translation-ready, block-styles, wide-blocks
Requires at least: 5.7
Tested up to: 6.0
Requires PHP: 7.3
License: GPL-3.0-or-later
License URI: https://www.gnu.org/licenses/license-list.html#GNUGPLv3

Acumen WordPress Theme, Copyright 2021 FireFly Themes
Acumen Theme is distributed under the terms of the GNU GPL

== Description ==

Acumen is a highly customizable and versatile multipurpose WordPress theme that is perfect for any kind of website. Whether you are creating a business website, an online store, a personal blog, or a portfolio, Acumen has the features and functionality to make your vision a reality. One of the key features of Acumen is its responsive design, which ensures that your website looks great on any device. It also includes powerful sections for homepage that makes it easy to create custom layouts and add various elements to your pages, such as sliders, galleries, and forms. In addition, Acumen includes a range of pre-designed templates and demos that can be imported with a single click, giving you a head start on creating your website. These templates cover a wide range of styles and niches, so you can choose the one that best fits your brand and aesthetic. Overall, Acumen is a comprehensive and user-friendly WordPress theme that is suitable for any kind of website. Its versatility, customization options, and responsive design make it a great choice for anyone looking to create a professional and stylish online presence. Visit https://fireflythemes.com/support for support, https://fireflythemes.com/documentation/acumen for theme documentation and https://demo.fireflythemes.com/acumen for demo.

== Support ==
Support can be found at https://fireflythemes.com/support

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Acumen includes support for Infinite Scroll in Jetpack, Breadcrunb NXT for advanced breadcrumb solution.

== Changelog ==

= 1.0.0 - 9 January 2023 =
* Initial release

== Credits ==

* Based on Underscores https://underscores.me/, (C) 2012-2020 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css https://necolas.github.io/normalize.css/, (C) 2012-2016 Nicolas Gallagher and Jonathan Neal, [MIT](https://opensource.org/licenses/MIT)
* Font Awesome Free 5.15.1 by @fontawesome - https://fontawesome.com
	* License - https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License)
* Select2 https://select2.github.io, (C) 2012-2017 Kevin Brown, Igor Vaynberg, and Select2 contributors [MIT] (https://opensource.org/licenses/MIT)
* Customizer Custom Controls, https://github.com/maddisondesigns/customizer-custom-controls, [GPLv2](http://www.gnu.org/licenses/gpl-2.0.html)
* Swiper http://swiperjs.com (C) 2014-2019 Vladimir Kharlampidi [MIT] (https://opensource.org/licenses/MIT)
* Customizer Reset Button: [GPLv2] https://wordpress.org/plugins/customizer-reset-by-wpzoom/ [GPLv2]
* Webfonts Loader: [MIT] https://github.com/WPTT/webfont-loader

Images
	All images are licensed under [CC0] (https://creativecommons.org/publicdomain/zero/1.0/)
		* Screenshot and header image:

		License : CC0 1.0 Universal (CC0 1.0)
		License URI : https://stocksnap.io/license
		 * https://stocksnap.io/photo/woman-working-UXMLQGWQB8
		* https://stocksnap.io/photo/woman-mobile-Q93BUD2Z3Z

	* All other images are made/shot by Firefly Themes and released under theme license.




	