<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package shuttle-ecommerce
 */

if(get_theme_mod('shuttle_ecommerce_display_breadcrumb_section',true) != ''){
	shuttle_ecommerce_breadcrumb_slider();
}elseif(get_post_type()){	
	if(get_post_meta(get_the_ID(),'breadcrumb_select',true) == 'yes'){
		shuttle_ecommerce_breadcrumb_slider();
	}
}