<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package shuttle-ecommerce
 */
?>
<div class="woocommerce_product_sections">
	<div class="shuttle_ecommerce_product_data">
		<?php dynamic_sidebar('woocommerce_product'); ?>
	</div>		
</div>