<?php

function shuttle_ecommerce_about_menu() {
	add_theme_page( esc_html__( 'About Theme', 'shuttle-ecommerce' ), esc_html__( 'About Theme', 'shuttle-ecommerce' ), 'edit_theme_options', 'shuttle-ecommerce-about', 'shuttle_ecommerce_about_display' );
}
add_action( 'admin_menu', 'shuttle_ecommerce_about_menu' );

function shuttle_ecommerce_about_display(){
	?>
	<div class="shuttle_ecommerce_about_data">
		<div class="shuttle_ecommerce_about_title">
			<h1><?php echo esc_html__( 'Welcome to Shuttle Ecommerce!', 'shuttle-ecommerce' ); ?></h1>
			<div class="shuttle_ecommerce_about_theme">
				<div class="shuttle_ecommerce_about_description">
					<p>
						<?php echo esc_html__( 'Shuttle Ecommerce is a powerful and multipurpose WordPress Theme. This theme has robust and flexible features with multiple customizable features, your website can gain a unique look. This multipurpose theme is packed with a lot of awesome features that can be used for various kinds of websites with focus on business sites. Shuttle Ecommerce has features like Social Icon, Transparent Header, featured slider, featured Section, About Section, Our Portfolio, Our team Section, Testimonial Slider, Our Services, Our Sponsors, Sticky Header, Social Information, Sidebar, Excerpt Options, and any eCommerce business need. This theme is supported for WooCommerce. All of these highly-customizable features and sections are completely responsive and absolutely easy to customize. If you are searching for unique looking flexible theme then Shuttle Ecommerce can be your ultimate choice to showcase your business. ', 'shuttle-ecommerce' ); 
						?>						
					</p>
				</div>
				<div class="shuttle_ecommerce_about_image">
					<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/screenshot.png">
				</div> 
			</div>
			<div class="shuttle_ecommerce_about_demo">
				<div class="feature-section">
					<div class="about_data_shuttle_ecommerce">
						<h3><?php echo esc_html__( 'Documentation', 'shuttle-ecommerce' ); ?></h3>
						<p><?php echo esc_html__( 'Getting started with a new theme can be difficult, But its installation and customization is so easy', 'shuttle-ecommerce' ); ?></p>
						<a href="https://www.xeeshop.com/shuttle-ecommerce-documentation/"><?php echo esc_html( 'Read Documentation', 'shuttle-ecommerce' ); ?></a>
					</div>
				</div>
				<div class="feature-section">
					<div class="about_data_shuttle_ecommerce">
						<h3><?php echo esc_html__( 'Recommended Plugins', 'shuttle-ecommerce' ); ?></h3>
						<p><?php echo esc_html__( 'Please install recommended plugins for better use of theme. It will help you to make website more useful', 'shuttle-ecommerce' ); ?></p>
						<a href="<?php echo esc_url(admin_url('/themes.php?page=tgmpa-install-plugins&plugin_status=activate'), 'shuttle-ecommerce'); ?>"><?php echo esc_html( 'Install Plugins ', 'shuttle-ecommerce' ); ?></a>
					</div>
				</div>
				<div class="feature-section">
					<div class="about_data_shuttle_ecommerce">
						<h3><?php echo esc_html__( 'Free Theme Demo', 'shuttle-ecommerce' ); ?></h3>
						<p><?php echo esc_html__( 'You can check free theme demo before setup your website if you like demo then install theme', 'shuttle-ecommerce' ); ?></p>
						<a href="https://xeeshop.com/themedemo/shuttle-ecommerce/"><?php echo esc_html( 'Free Theme Demo ', 'shuttle-ecommerce' ); ?></a>
					</div>
				</div>
				<div class="feature-section">
					<div class="about_data_shuttle_ecommerce">
						<h3><?php echo esc_html__( 'Free VS Pro', 'shuttle-ecommerce' ); ?></h3>
						<p><?php echo esc_html__( 'You can check compare free version and pro version.', 'shuttle-ecommerce' ); ?></p>
						<a href="https://www.xeeshop.com/product/shuttle-ecommerce-pro/"><?php echo esc_html( 'Compare free Vs Pro ', 'shuttle-ecommerce' ); ?></a>
					</div>
				</div>
				<div class="feature-section">
					<div class="about_data_shuttle_ecommerce">
						<h3><?php echo esc_html__( 'Rate this theme', 'shuttle-ecommerce' ); ?></h3>
						<p><?php echo esc_html__( 'If you like our theme, Please vote us , so we can contribute more features for you.', 'shuttle-ecommerce' ); ?></p>
						<a href="#"><?php echo esc_html( 'Rate This Theme ', 'shuttle-ecommerce' ); ?></a>
					</div>
				</div>
			</div>
		</div>
		<ul class="tabs">
			<li class="tab-link current" data-tab="about"><?php echo esc_html__( 'About', 'shuttle-ecommerce' ); ?></li>
		</ul> 
		<div id="about" class="tab-content current">
			<div class="about_section">
				<div class="about_info_data theme_info">
					<div class="about_theme_title">
						<h2><?php echo esc_html__( 'Theme Customizer', 'shuttle-ecommerce' ); ?></h2>
					</div>
					<div class="about_theme_data">
						<p><?php echo esc_html__( 'All Theme Options are available via Customize screen.', 'shuttle-ecommerce' ); ?></p>
					</div>
					<div class="about_theme_btn">
						<a class="customize_btn button button-primary" href="<?php echo esc_url( admin_url( 'customize.php' ) ); ?>"><?php echo esc_html__( 'Customize', 'shuttle-ecommerce' ); ?></a>
					</div>
				</div>
				<div class="theme_que theme_info">
					<div class="about_theme_que">
						<h2><?php echo esc_html__( 'Got theme support question?', 'shuttle-ecommerce' ); ?></h2>
					</div>
					<div class="about_que_data">
						<p><?php echo esc_html__( 'Get genuine support from genuine people. Whether it is customization or compatibility, our seasoned developers deliver tailored solutions to your queries.', 'shuttle-ecommerce' ); ?></p>
					</div>
					<div class="about_que_btn">
						<a class="support_forum button button-primary" href="https://www.xeeshop.com/support-us/"><?php echo esc_html__( 'Support Forum', 'shuttle-ecommerce' ); ?></a>
					</div>
				</div>
			</div>
			<div class="about_shortcode theme_info">
				<div class="about_single_page_post_shortcode">
					<h2><?php echo esc_html__( 'Single Page And Post Add shortcode', 'shuttle-ecommerce' ); ?></h2>
					<p><?php echo esc_html__( 'if this plugin Page Section For Themereviewer must be installed then this Shortcode use Otherwise this Shortcode is not work.', 'shuttle-ecommerce' ); ?></p>
				</div>
				<ul>
					<h3><?php echo esc_html__( 'Featured Slider :', 'shuttle-ecommerce' ); ?></h3>
					<li><?php echo esc_html__( "[theme_section section='featured_slider_activate']", "shuttle-ecommerce" ); ?></li>

					<h3><?php echo esc_html__( 'Featured Section :', 'shuttle-ecommerce' ); ?></h3>
					<li><?php echo esc_html__( "[theme_section section='featured_section_info_activate']", "shuttle-ecommerce" ); ?></li>

					<h3><?php echo esc_html__( 'About Section :', 'shuttle-ecommerce' ); ?></h3>
					<li><?php echo esc_html__( "[theme_section section='about_section_activate']", "shuttle-ecommerce" ); ?></li>

					<h3><?php echo esc_html__( 'Our Portfolio :', 'shuttle-ecommerce' ); ?></h3>
					<li><?php echo esc_html__( "[theme_section section='our_portfolio_section_activate']", "shuttle-ecommerce" ); ?></li>

					<h3><?php echo esc_html__( 'Our Services :', 'shuttle-ecommerce' ); ?></h3>
					<li><?php echo esc_html__( "[theme_section section='our_services_activate']", "shuttle-ecommerce" ); ?></li>

					<h3><?php echo esc_html__( 'Our Sponsors :', 'shuttle-ecommerce' ); ?></h3>
					<li><?php echo esc_html__( "[theme_section section='our_sponsors_activate']", "shuttle-ecommerce" ); ?></li>

					<h3><?php echo esc_html__( 'Our Team :', 'shuttle-ecommerce' ); ?></h3>
					<li><?php echo esc_html__( "[theme_section section='our_team_activate']", "shuttle-ecommerce" ); ?></li>

					<h3><?php echo esc_html__( 'Our Testimonial :', 'shuttle-ecommerce' ); ?></h3>
					<li><?php echo esc_html__( "[theme_section section='our_testimonial_activate']", "shuttle-ecommerce" ); ?></li>

					<h3><?php echo esc_html__( 'Widget Section :', 'shuttle-ecommerce' ); ?></h3>
					<li><?php echo esc_html__( "[theme_section section='woocommerce_product_section_activate']", "shuttle-ecommerce" ); ?></li>
				</ul>
			</div>
		</div>
	</div>
	<?php
}