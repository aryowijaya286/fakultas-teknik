<?php
function shuttle_ecommerce_customize_css(){
	global $default_setting;
	global $shuttle_ecommerce_fonttotal;
	$shuttle_ecommerce_body_fontfamily = get_theme_mod("shuttle_ecommerce_body_fontfamily",5);
    $shuttle_ecommerce_body_fontfamily = $shuttle_ecommerce_fonttotal[$shuttle_ecommerce_body_fontfamily];

    $shuttle_ecommerce_Heading_fontfamily = get_theme_mod("shuttle_ecommerce_Heading_fontfamily",5);
    $shuttle_ecommerce_Heading_fontfamily = $shuttle_ecommerce_fonttotal[$shuttle_ecommerce_Heading_fontfamily];

    $shuttle_ecommerce_Heading1_fontfamily = get_theme_mod("shuttle_ecommerce_Heading1_fontfamily",5);
    $shuttle_ecommerce_Heading1_fontfamily = $shuttle_ecommerce_fonttotal[$shuttle_ecommerce_Heading1_fontfamily];

    $shuttle_ecommerce_Heading2_fontfamily = get_theme_mod("shuttle_ecommerce_Heading2_fontfamily",5);
    $shuttle_ecommerce_Heading2_fontfamily = $shuttle_ecommerce_fonttotal[$shuttle_ecommerce_Heading2_fontfamily];

    $shuttle_ecommerce_Heading3_fontfamily = get_theme_mod("shuttle_ecommerce_Heading3_fontfamily",5);
    $shuttle_ecommerce_Heading3_fontfamily = $shuttle_ecommerce_fonttotal[$shuttle_ecommerce_Heading3_fontfamily];

    //Body Font-Family
    if($shuttle_ecommerce_body_fontfamily!='Select Font'){
		?>
		<style type="text/css">
	        body{
	            font-family: <?php echo esc_attr( $shuttle_ecommerce_body_fontfamily );?>            
	        }
        </style>
        <?php
    }
    if($shuttle_ecommerce_Heading_fontfamily!='Select Font'){
    	?>
		<style type="text/css">
	        h1, h2, h3, h4, h5, h6{
	            font-family: <?php echo esc_attr( $shuttle_ecommerce_Heading_fontfamily );?>            
	        }
        </style>
        <?php
    }
    if($shuttle_ecommerce_Heading1_fontfamily!='Select Font'){
    	?>
		<style type="text/css">
	        h1{
	            font-family: <?php echo esc_attr( $shuttle_ecommerce_Heading1_fontfamily );?>            
	        }
        </style>
        <?php
    }
    if($shuttle_ecommerce_Heading2_fontfamily!='Select Font'){
    	?>
		<style type="text/css">
	        h2{
	            font-family: <?php echo esc_attr( $shuttle_ecommerce_Heading2_fontfamily );?>            
	        }
        </style>
        <?php
    }
    if($shuttle_ecommerce_Heading3_fontfamily!='Select Font'){
    	?>
		<style type="text/css">
	        h3{
	            font-family: <?php echo esc_attr( $shuttle_ecommerce_Heading3_fontfamily );?>            
	        }
        </style>
        <?php
    }
    if(get_theme_mod('shuttle_ecommerce_header_layout','header1')=='header1'){
    	?>
		<style type="text/css">
			.main_site_header{
				background-color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_header1_bg_color','#133b41')); ?>;
				color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_header1_text_color','#ffffff')); ?>;
			}
			.main_site_header a{
				color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_header1_Link_color','#ffffff')); ?>;
			}
			.main_site_header a:hover{
				color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_header1_linkhover_color','#d7d4d4')); ?>;
			}
	    	.header_info {
		        /*display: grid;
			    grid-template-columns: auto auto;			    
			    grid-column-gap: 20px;
			    overflow-wrap: anywhere;*/			    
			    flex-wrap: wrap;
			    display: flex;
			    align-items: center;
			    justify-content: space-between;
			    /*padding: 12px 10px;		*/	    
			}
			.header_info p.site-description, .header_info h1.site-title {
			    margin: 0px;
			}
			.call_button_info {
			    display: grid;
			    grid-template-columns: auto auto auto auto;
			    align-items: center;
			    overflow-wrap: anywhere;
			    justify-content: space-between;
			}
			.search_call_btn_info {
			    display: flex;
			}
			.top_header{
				background-color: <?php echo esc_attr(get_theme_mod('header1_top_bar_bg_color','#fff')); ?>;
				color: <?php echo esc_attr(get_theme_mod('header1_top_bar_text_color','#133b41')); ?>;
			}
		</style>
		<?php
    }
    
    if(get_theme_mod('shuttle_ecommerce_header_layout','header1')=='header3'){
    	?>
		<style type="text/css">
			header#masthead {
				/*background: <?php echo esc_attr(get_theme_mod('transparent_header_bg_color','rgba(255,255,255,0.3)')); ?>;*/
			    position: absolute;
			    right: 0;
			    left: 0;
			    width: 100%;
			    border-top: 0;
			    margin: 0 auto;
			    z-index: 99;
			    color: <?php echo esc_attr(get_theme_mod('transparent_header_text_color','#fff')); ?>;
			}
			header#masthead a{
				color: <?php echo esc_attr(get_theme_mod('transparent_header_link_color','#fff')); ?>;
			}
			header#masthead a:hover{
				color: <?php echo esc_attr(get_theme_mod('transparent_header_link_hover_color','#02cfaa')); ?>;
			}
			.main_site_header {
				background: <?php echo esc_attr(get_theme_mod('transparent_header_bg_color','rgba(255,255,255,0.3)')); ?>;
			}
			/*.top_header {
			    background-color: unset !important;
			}*/
			.top_header{
				background-color: <?php echo esc_attr(get_theme_mod('transparent_header_topbar_bg_color','rgba(255,255,255,0.3)')); ?>;
				color: <?php echo esc_attr(get_theme_mod('transparent_header_topbar_text_color','#ffffff')); ?>;
			}
			.featured_slider_image .hentry-inner {
			    height: 700px;
			}
			.header_info {
				display: flex;
				flex-wrap: wrap;
    			flex-direction: row;
    			align-items: center;
			    overflow-wrap: anywhere;
			    justify-content: space-between;
			}
			.call_button_info {
			    display: grid;
			    grid-template-columns: auto auto auto;
			}
			.home .featured_slider_image .hentry-inner .entry-container{
				margin-top: 180px;
			}
			.site-branding h1.site-title, .site-branding p {
			    margin: 5px 0px;
			}
			.breadcrumb_info {
			    padding-top: 240px;
			    padding-bottom: 90px;
			}
		</style>
		<?php
    }
    if(get_theme_mod( 'shuttle_ecommerce_header_width_layout','content_width')=='content_width'){
    	?>
		<style type="text/css">
    		.header_info {
    			max-width: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_header_contact_width','1100')); ?>px;
			    margin-left: auto;
			    margin-right: auto;
    		}
    	</style>
		<?php
    }
    if(get_theme_mod( 'shuttle_ecommerce_top_bar_width_layout','content_width')=='content_width'){
    	?>
		<style type="text/css">
    		.topbar_info_data {
    			max-width: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_top_bar_contact_width','1100')); ?>px;
			    margin-left: auto;
			    margin-right: auto;
    		}
    	</style>
		<?php
    }
    if(get_theme_mod( 'shuttle_ecommerce_footer_width_layout','content_width')=='content_width'){
    	?>
		<style type="text/css">
    		footer#colophon .site-info {
    			max-width: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_footer_contact_width','1100')); ?>px;
			    margin-left: auto;
			    margin-right: auto;
    		}
    	</style>
		<?php
    }
    if(get_theme_mod( 'display_cart_icon',true) == ''){
    	?>
		<style type="text/css">
			.add_cart_icon{
				display: none !important;
			}
		</style>
		<?php
    }
    if(get_theme_mod( 'display_mobile_cart_icon',true) == ''){
		?>
		<style type="text/css">
			@media only screen and (max-width: 768px){
				.add_cart_icon{
					display: none !important;
				}
			}
		</style>
		<?php
	}
	if(get_theme_mod( 'display_mobile_cart_icon',true) == true){
		?>
		<style type="text/css">
			@media only screen and (max-width: 768px){
				.add_cart_icon{
					display: block !important;
				}
			}
		</style>
		<?php
	}
	if(get_theme_mod( 'display_mobile_search_icon', true) == true){
		?>
		<style type="text/css"> 
			@media only screen and (max-width: 768px){
				div#cl_serchs{
					display: block !important;
				}
			}
		</style>
		<?php
	}
	if(get_theme_mod( 'display_mobile_search_icon', true) == ''){
		?>
		<style type="text/css"> 
			@media only screen and (max-width: 768px){
				div#cl_serchs{
					display: none;
				}
			}
		</style>
		<?php
	}
	if(get_theme_mod( 'display_search_icon',true) == ''){ 
    	?>
		<style type="text/css">
			div#cl_serchs {
			   display: none;
			}
		 </style>
        <?php
    }	
	if(get_theme_mod( 'shuttle_ecommerce_post_sidebar_width_'.get_post_type(),'30')){
    	$secondary_width = get_theme_mod('shuttle_ecommerce_post_sidebar_width_'.get_post_type(),'30');
		$primary_width   = absint( 100 - $secondary_width );
		?>
		<style type="text/css">
			aside.widget-area{
				width: <?php echo esc_attr($secondary_width);?>%;
			}
			main#primary{
				width: <?php echo esc_attr($primary_width);?>%;
			}
		</style>
		<?php
	}
    if(get_theme_mod( 'shuttle_ecommerce_container_width_layout','content_width')=='content_width'){
    	?>
		<style type="text/css">
		    .shuttle_ecommerce_container_info{
		    	max-width: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_container_contact_width','1100')); ?>px;
			    margin-left: auto;
			    margin-right: auto;
			    padding: 20px 0px;
		    }
		    main#primary{		    	
		    	margin: 15px;
		    }
		    main#primary {
			    background-color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_boxed_container_bg_color','#eeeeee')); ?>;
			}
		    aside.widget-area .widget{
		    	background-color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_boxed_container_bg_color','#eeeeee')); ?>;
		    	margin: 15px;
		    }
    	</style>
		<?php
    }
    if(get_theme_mod( 'shuttle_ecommerce_container_width_layout','content_width')=='boxed_layout'){
    	?>
		<style type="text/css">
		    .shuttle_ecommerce_container_info{
		    	max-width: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_container_contact_width','1100')); ?>px;
			    margin-left: auto;
			    margin-right: auto;
		    }
		    .shuttle_ecommerce_container_info {
			    margin-top: 20px;
			    margin-bottom: 20px;
			}
			.blog .shuttle_ecommerce_container_info.boxed_layout{
				background-color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_boxed_layout_bg_color','#eeeeee')); ?>;
			}
			.blog .shuttle_ecommerce_container_info.boxed_layout .main_containor article{
				margin-bottom: 20px;
			}
			aside.widget-area {
			    margin: 10px;
			    padding: 0px;
			}
			aside.widget-area .widget{
				margin: 0px 0px 15px 0px;
				background-color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_boxed_layout_bg_color','#eeeeee')); ?>;
			}
			main#primary{
				padding: 15px;
			    margin: 10px;
			    background: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_boxed_layout_bg_color','#eeeeee')); ?>;
			}
			.featured-section, .about_data, .our_portfolio_data, .our_team_info, .our_services_info, .our_testimonial_info, .our_sponsors_info, .shuttle_ecommerce_product_data {
				max-width: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_container_contact_width','1100')); ?>px;
			    margin-left: auto;
			    margin-right: auto;
			}
		</style>
		<?php
    }
    if(get_theme_mod( 'shuttle_ecommerce_container_width_layout','content_width')=='full_width'){
    	?>
    	<style type="text/css">
    		.shuttle_ecommerce_container_info.full_width .main_containor article{
				margin-bottom: 20px;
			}
    	</style>
    	<?php
    }
    if(get_theme_mod( 'shuttle_ecommerce_breadcrumb_bg_image')){
    	?>
		<style type="text/css">
		.breadcrumb_info{
			background: url(<?php echo esc_attr(get_theme_mod('shuttle_ecommerce_breadcrumb_bg_image'))?>) rgb(0 0 0 / 0.75);
			background-position: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_img_bg_position','center center')); ?>;
		    background-attachment: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_breadcrumb_bg_attachment','scroll'));?>;
		    background-size: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_breadcrumb_bg_size','cover'));?>;
		    background-blend-mode: multiply;
		}
		</style>
		<?php
    }else{
    	?>
		<style type="text/css">
    	.breadcrumb_info{
			background-color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_breadcrumb_bg_color','#c8c9cb')); ?>;
		}
		</style>
		<?php
    }
	if(get_theme_mod('shuttle_ecommerce_container_width_layout','content_width') == 'content_width'){
		?>
		<style type="text/css">
			.featured-section, .about_data, .our_portfolio_data, .our_team_info, .our_services_info, .our_testimonial_info, .our_sponsors_info, .shuttle_ecommerce_product_data {
				max-width: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_container_contact_width','1100')); ?>px;
			    margin-left: auto;
			    margin-right: auto;
			}
		</style>
		<?php
	}
    if(get_theme_mod( 'display_scroll_button',true) == ''){
		?>
		<style>			
			.scrolling-btn {
    			display: none;
			}	   
		</style>
		<?php
	}
	if(get_theme_mod( 'shuttle_ecommerce_container_containe',true ) == ''){
    	?>
		<style type="text/css">
	    	.blog .shuttle_ecommerce_container_data {
			    display: none;
			}
	    </style>
        <?php
    } 
   	if(get_theme_mod( 'shuttle_ecommerce_container_description',false ) == ''){
    	?>
		<style type="text/css">
	    	.blog article .entry-content {
			    display: none;
			}
	    </style>
        <?php
    }
    if(get_theme_mod( 'shuttle_ecommerce_container_date',true ) == ''){
    	?>
		<style type="text/css">
	    	.blog span.posted-on {
			    display: none;
			}
	    </style>
        <?php
    }
    if(get_theme_mod( 'shuttle_ecommerce_container_authore',false ) == ''){
    	?>
		<style type="text/css">
			.blog span.byline {
				display: none;
			}
		 </style>
        <?php
    }
    if(get_theme_mod( 'shuttle_ecommerce_container_category',true ) == ''){
    	?>
		<style type="text/css">
			.blog span.cat-links {
				display: none;
			}
		 </style>
        <?php
    } 
    if(get_theme_mod( 'shuttle_ecommerce_container_comments',false ) == ''){
    	?>
		<style type="text/css">
			.blog span.comments-link {
				display: none;
			}
		 </style>
        <?php
    } 
    
    if(get_theme_mod('footer_bg_image')){
    	?>
		<style type="text/css">
			footer#colophon{
				background:url(<?php echo  esc_attr(get_theme_mod('footer_bg_image'));?>) rgb(0 0 0 / 0.75);
	    		background-position: <?php echo esc_attr(get_theme_mod('footer_bg_position','center center')); ?>;
	    		background-size: <?php echo esc_attr(get_theme_mod('footer_bg_size','cover')); ?>;
	    		background-attachment: <?php echo esc_attr(get_theme_mod('footer_bg_attachment','scroll')); ?>;
	    		background-blend-mode: multiply;
			}
		</style>
		<?php
    }else{
    	?>
		<style type="text/css">
			footer#colophon{
				background-color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_footer_bg_color','#133b41')); ?>;
			}
		</style>
		<?php
    }
    
	?>
	<style>
		.featured_slider_image .hentry-inner .entry-container {
		    background-color: <?php echo esc_attr(get_theme_mod('alpha_color_setting','rgba(0,0,0,0.5)')); ?>;
		}
		.main-navigation li:before{
			background-color: <?php echo esc_attr(get_theme_mod('header_menu_active_hover_bg_color','#02cfaa ')); ?>;
		}
		body {
			font-size: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_body_font_size','15')); ?>px;
			font-weight: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_body_font_weight','400')); ?>;
			text-transform: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_body_text_transform','inherit')); ?>;
		}
		h1{
			font-size: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_heading1_font_size','35')); ?>px;
			font-weight: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_heading1_font_weight','bold')); ?>;
			text-transform: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_heading1_text_transform','inherit')); ?>;
		}
		h2{
			font-size: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_heading2_font_size','28')); ?>px;
			font-weight: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_heading2_font_weight','bold')); ?>;
			text-transform: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_heading2_text_transform','inherit')); ?>;
		}
		h3{
			font-size: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_heading3_font_size','25')); ?>px;
			font-weight: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_heading3_font_weight','400')); ?>;
			text-transform: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_heading3_text_transform','inherit')); ?>;
		}
		body a, time.entry-date.published:before, time.entry-date.published:before, span.cat-links:before, span.comments-link:before, span.byline:before {
			color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_link_color','#133b41')); ?> ;
			text-decoration: none;
		} 
		body a:hover {
			color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_link_hover_color','#000000')); ?> ;
		}
		.blog main#primary {
		    background: none;
		}
		.blog .shuttle_ecommerce_container_info.content_width .main_containor article{
			background-color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_boxed_container_bg_color','#eeeeee')); ?>;
			margin-bottom: 20px;
		}
		aside.widget-area section h2, aside.widget-area label.wp-block-search__label{
			background-color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_sidebar_heading_bg_color','#133b41')); ?>;
			color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_sidebar_heading_text_color','#ffffff')); ?>;
		}
		span.separator:before, .separator > span:before, .separator > span:after, span.separator:after{
			background-color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_heading_underline_color','#133b41')); ?>;
		}
		/*.separator span span{
			border: 2px solid <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_heading_underline_color','#133b41')); ?>;
		}*/
		.current-menu-ancestor > a, .current-menu-item > a, .current_page_item > a {
			color: <?php echo esc_attr(get_theme_mod('header_menu_active_color','#133b41 ')); ?> !important;
		}
		li.current-menu-item {
		    background-color: <?php echo esc_attr(get_theme_mod('header_menu_active_hover_bg_color','#02cfaa ')); ?>;
		}
		.main-navigation .nav-menu ul.sub-menu{
			background-color: <?php echo esc_attr(get_theme_mod('header_desktop_submenu_bg_color','#00aa94')); ?>;
		}
		.main-navigation ul ul a{
			color: <?php echo esc_attr(get_theme_mod('header_desktop_submenu_text_color','#ffffff')); ?> !important;
		}
		a.social_icon i{
			background-color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_social_icon_bg_color','#133b41')); ?>;
			color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_social_icon_color','#ffffff')); ?>;
			border-color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_social_icon_bg_color','#133b41')); ?>;
		}
		a.social_icon i:hover{
			background-color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_social_icon_bg_hover_color','#ffffff')); ?>;
			color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_social_icon_hover_color','#133b41')); ?>;
			border-color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_social_icon_bg_hover_color','#ffffff')); ?>;
		}
		img.custom-logo {
		    width: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_logo_width','150')); ?>px;
		}
		.main-navigation ul#primary-menu > li > ul:after {
			border-bottom: 9px solid <?php echo esc_attr(get_theme_mod('header_desktop_submenu_bg_color','#00aa94')); ?>;
		}

		/*--------------------------------------------------------------
		# Buttons
		--------------------------------------------------------------*/
		button, input[type="button"], input[type="reset"], input[type="submit"], .wp-block-search .wp-block-search__button,.nav-previous a, .nav-next a, .buttons, .woocommerce a.button, .woocommerce button, .woocommerce .single-product button, .woocommerce button.button.alt, .woocommerce a.button.alt, .woocommerce button.button,.woocommerce button.button.alt.disabled {
			display: inline-block;
			background-color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_button_bg_color','#133b41')); ?>;
			color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_button_text_color','#ffffff')); ?>!important ;			
			padding: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_button_padding','10px 15px')); ?> ;
			border: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_borderwidth','2')); ?>px solid <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_button_border_color','#133b41')); ?>;
			border-radius:  <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_button_border_radius','2')); ?>px;
		}
		button:hover, input[type="button"]:hover , input[type="reset"]:hover , input[type="submit"]:hover , .wp-block-search .wp-block-search__button:hover, .nav-previous a:hover, .buttons:hover, .nav-next a:hover, .woocommerce a.button:hover, .woocommerce button:hover, .woocommerce .single-product button:hover, .woocommerce button.button.alt:hover, .woocommerce a.button.alt:hover, .woocommerce button.button:hover, .woocommerce button.button.alt.disabled:hover{
			color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_button_texthover_color','#133b41')); ?>!important ;
			background-color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_button_bg_hover_color','#ffffff')); ?>;
			border: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_borderwidth','2')); ?>px solid <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_button_border_hover_color','#133b41')); ?>;
			 box-shadow: inset 0 0 0 2em <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_button_bg_hover_color','#ffffff')); ?>;
		}
		.woocommerce .woocommerce-message {
		    border-top-color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_button_bg_color','#133b41')); ?> 
		}
		.woocommerce .woocommerce-message::before{
			color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_button_bg_color','#133b41')); ?> ;
		}
		.woocommerce .woocommerce-info, .woocommerce-noreviews, p.no-comments {
		    background-color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_button_bg_color','#133b41')); ?> ;
			color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_button_text_color','#ffffff')); ?> ;
		}
		/*--------------------------------------------------------------
		# buttons end
		--------------------------------------------------------------*/

		.call_menu_btn{
			background-color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_callmenu_btn_bg_color','#133b41')); ?>;
			color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_callmenu_btn_color','#fff')); ?> !important;
			border: 1px solid  <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_call_btn_border_color','#ffffff')); ?>;
		}
		.call_menu_btn:hover{
			background-color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_callmenu_btn_bghover_color','#ffffff')); ?>;
			color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_call_btn_texthover_color','#133b41')); ?> !important;
		}
		.shuttle_ecommerce_container_data {
		    background-color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_container_bg_color','#ffffff')); ?>;
		}
		.main_containor.grid_view{
		    display: grid;
		    grid-template-columns: repeat(<?php echo esc_attr(get_theme_mod('shuttle_ecommerce_container_grid_view_col','3'));?>, 1fr);
		    grid-column-gap :<?php echo esc_attr(get_theme_mod('shuttle_ecommerce_container_grid_view_col_gap','20'));?>px;
		}
		/*--------------------------------------------------------------
		# breadcrumb Section
		--------------------------------------------------------------*/
		.breadcrumb_info{
			color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_breadcrumb_text_color','#ffffff')); ?>;
		}
		section#breadcrumb-section a {
		    color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_breadcrumb_link_color','#ffffff')); ?>;
    		text-decoration: none;
    		border: 2px solid <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_breadcrumb_link_color','#ffffff')); ?>;
    		padding: 7px;
    		border-radius: 100px;
		}
		.breadcrumb_info ol.breadcrumb-list {
			background-color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_breadcrumb_icon_background_color','#133b41'));?>;
			border: 1px solid <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_breadcrumb_icon_background_color','#133b41'));?>;
		}

		/*--------------------------------------------------------------
		# Featured Section Start
		--------------------------------------------------------------*/
			.section-featured-wrep .featured-icon {
				border: 1px solid <?php echo esc_attr(get_theme_mod('featured_section_icon_bg_color',$default_setting['featured_section_icon_bg_color'])); ?>;
				background: <?php echo esc_attr(get_theme_mod('featured_section_icon_bg_color',$default_setting['featured_section_icon_bg_color'])); ?>;	
				color: <?php echo esc_attr(get_theme_mod('featured_section_icon_color',$default_setting['featured_section_icon_color'])); ?>;
			}
			.section-featured-wrep:hover .featured-icon {
				background: <?php echo esc_attr(get_theme_mod('featured_section_icon_bg_hover_color',$default_setting['featured_section_icon_bg_hover_color'])); ?>;	
				color: <?php echo esc_attr(get_theme_mod('featured_section_icon_hover_color',$default_setting['featured_section_icon_hover_color'])); ?>;
				border: 1px solid <?php echo esc_attr(get_theme_mod('featured_section_icon_bg_hover_color',$default_setting['featured_section_icon_bg_hover_color'])); ?>;
			}
			.featured_content .section-featured-wrep i {
			    border: none;
			    background: unset;
			    color: unset;
			}
			.featured_content .section-featured-wrep:hover i {
			    background: unset;	
				color: unset;
			}
		/*--------------------------------------------------------------
		# Featured Section end
		--------------------------------------------------------------*/

		.our_services_section .card:hover {
		    color: <?php echo esc_attr(get_theme_mod('our_services_contain_text_hover_color','#133b41'));?>;
		}
		.card:before{
			background-color: <?php echo esc_attr(get_theme_mod('our_services_contain_bg_hover_color',$default_setting['our_services_contain_bg_hover_color']));?>;
		}
		

		/*--------------------------------------------------------------
		# Testimonials
		--------------------------------------------------------------*/
			.image_testimonials {
			    background: unset !important;
			}

		/*--------------------------------------------------------------
		# Footer
		--------------------------------------------------------------*/
		footer#colophon{
			color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_footer_text_color','#ffffff')); ?>;
			padding: 10px;
		}
		footer#colophon a{
			color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_footer_link_color','#02cfaa')); ?> !important;
		}
		footer#colophon a:hover{
			color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_footer_link_hover_color','#ffffff')); ?>;
		}
		.scrolling-btn{
			background-color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_scroll_button_bg_color','#02cfaa'));?> !important;
			color: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_scroll_button_color','#ffffff')); ?> !important;
		}

		@media only screen and (max-width: 768px){
			body {
				font-size: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_mobile_font_size','14')); ?>px;
			} 
			h1{
				font-size: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_mobile_heading1_font_size','20')); ?>px;
			} 
			h2{
				font-size: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_mobile_heading2_font_size','18')); ?>px;
			}
			h3{
				font-size: <?php echo esc_attr(get_theme_mod('shuttle_ecommerce_mobile_heading3_font_size','14')); ?>px;
			}
			.mobile_menu {
			    background-color: <?php echo esc_attr(get_theme_mod('header_mobile_navmenu_background_color','#133b41')); ?>;
			}
			.main-navigation .sub-menu li, .main-navigation ul ul ul.toggled-on li {
		        background-color: <?php echo esc_attr(get_theme_mod('header_mobile_submenu_background_color','#a9d7ce')); ?>;
		    }
		    .mobile_menu #primary-menu li a{
		    	color: <?php echo esc_attr(get_theme_mod('header_mobile_navmenu_color','#ffffff')); ?>;		    	
		    }
		    .current-menu-ancestor > a, .current-menu-item > a, .current_page_item > a {
			    color: <?php echo esc_attr(get_theme_mod('header_mobile_navmenu_active_color','#00ffca')); ?> !important;
			}
			.main-navigation ul#primary-menu > li > ul:after{
				border-bottom: 9px solid <?php echo esc_attr(get_theme_mod('header_mobile_submenu_background_color','#a9d7ce')); ?> !important;
			}
		}

	</style>
	<?php
	if (!class_exists('WooCommerce'))  return;
    //if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
		if(is_product() || is_shop() || is_cart() || is_checkout()){
			if(empty(get_post_meta(get_the_ID(),'sidebar_select',true))){
		        ?>
		        <style> 
			        aside.widget-area{
			            display: none;
			        }
			        main#primary {
					    width: 100% !important;
					}
		        </style>
		        <?php
		    }
	    }
	//}
}
add_action( 'wp_head', 'shuttle_ecommerce_customize_css');