<?php
/* Notifications in customizer */

require get_template_directory()  . '/inc/customizer-notify/shuttle-ecommerce-notify.php';
$shuttle_ecommerce_config_customizer = array(
	'recommended_plugins'       => array(
		'spediex-for-theme' => array(
			'recommended' => true,
			'description' => sprintf(__('Install and activate <strong>Spediex For Theme </strong> plugin for taking full advantage of all the features this theme has to offer shuttle ecommerce.', 'shuttle-ecommerce')),
		),
	),
	'recommended_actions'       => array(),
	'recommended_actions_title' => esc_html__( 'Recommended Actions', 'shuttle-ecommerce' ),
	'recommended_plugins_title' => esc_html__( 'Recommended Plugin', 'shuttle-ecommerce' ),
	'install_button_label'      => esc_html__( 'Install and Activate', 'shuttle-ecommerce' ),
	'activate_button_label'     => esc_html__( 'Activate', 'shuttle-ecommerce' ),
	'shuttle_ecommerce_deactivate_button_label'   => esc_html__( 'Deactivate', 'shuttle-ecommerce' ),
);
shuttle_ecommerce_Customizer_Notify::init( apply_filters( 'shuttle_ecommerce_recommended_plugins', $shuttle_ecommerce_config_customizer ) );
