<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div class="container">
 *
 * @package Eventum Lite
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php echo esc_url( get_bloginfo( 'pingback_url' ) ); ?>">
<?php endif; ?>
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php
	if ( function_exists( 'wp_body_open' ) ) {
		wp_body_open();
	} else {
		do_action( 'wp_body_open' );
	}
?>
<a class="skip-link screen-reader-text" href="#Tab-Naviagtion">
<?php esc_html_e('Skip to content', 'eventum-lite' ); ?>
</a>
<?php
$eventum_lite_show_hdrtpinfo 	   			= esc_attr( get_theme_mod('eventum_lite_show_hdrtpinfo', false) ); 
$eventum_lite_hdrfrontslider_show 	  		= esc_attr( get_theme_mod('eventum_lite_hdrfrontslider_show', false) );
$eventum_lite_twobx_settings_show      		= esc_attr( get_theme_mod('eventum_lite_twobx_settings_show', false) );
$eventum_lite_show_welcomesection      		= esc_attr( get_theme_mod('eventum_lite_show_welcomesection', false) );
?>
<div id="SiteWrapper" <?php if( get_theme_mod( 'eventum_lite_layoutoption' ) ) { echo 'class="boxlayout"'; } ?>>
<?php
if ( is_front_page() && !is_home() ) {
	if( !empty($eventum_lite_hdrfrontslider_show)) {
	 	$innerpage_cls = '';
	}
	else {
		$innerpage_cls = 'innerpage_header';
	}
}
else {
$innerpage_cls = 'innerpage_header';
}
?>

<div id="masthead" class="site-header <?php echo esc_attr($innerpage_cls); ?> ">      
       
        <?php if( $eventum_lite_show_hdrtpinfo != ''){ ?> 
          <div class="hdrinfobar">
           <div class="container">             
            <?php $eventum_lite_contactno = get_theme_mod('eventum_lite_contactno');
                if( !empty($eventum_lite_contactno) ){ ?>              
                <div class="infobx">
                    <i class="fas fa-phone fa-rotate-90"></i>
                    <?php echo esc_html($eventum_lite_contactno); ?>
                </div>       
            <?php } ?>
            
             <?php $email = get_theme_mod('eventum_lite_emailinfo');
                if( !empty($email) ){ ?>                
                <div class="infobx">
                    <i class="far fa-envelope"></i>
                    <a href="<?php echo esc_url('mailto:'.sanitize_email($email)); ?>"><?php echo sanitize_email($email); ?></a>
                </div>            
            <?php } ?>           
         
             <div class="infobx">
                <div class="hdr-tp-social">                                                
					   <?php $eventum_lite_facebooklink = get_theme_mod('eventum_lite_facebooklink');
                        if( !empty($eventum_lite_facebooklink) ){ ?>
                        <a class="fab fa-facebook-f" target="_blank" href="<?php echo esc_url($eventum_lite_facebooklink); ?>"></a>
                       <?php } ?>
                    
                       <?php $eventum_lite_twitterlink = get_theme_mod('eventum_lite_twitterlink');
                        if( !empty($eventum_lite_twitterlink) ){ ?>
                        <a class="fab fa-twitter" target="_blank" href="<?php echo esc_url($eventum_lite_twitterlink); ?>"></a>
                       <?php } ?>
                
                      <?php $eventum_lite_linkedinlink = get_theme_mod('eventum_lite_linkedinlink');
                        if( !empty($eventum_lite_linkedinlink) ){ ?>
                        <a class="fab fa-linkedin" target="_blank" href="<?php echo esc_url($eventum_lite_linkedinlink); ?>"></a>
                      <?php } ?> 
                      
                      <?php $eventum_lite_instagramlink = get_theme_mod('eventum_lite_instagramlink');
                        if( !empty($eventum_lite_instagramlink) ){ ?>
                        <a class="fab fa-instagram" target="_blank" href="<?php echo esc_url($eventum_lite_instagramlink); ?>"></a>
                      <?php } ?> 
                  </div><!--end .hdr-tp-social-->
                </div><!--end .infobx-->   
                
        		 <div class="clear"></div> 
          </div><!-- .container -->  
      </div><!-- .hdrinfobar -->      
   <?php } ?>   

      <div class="LogoNavi-Panel">  
       <div class="container">    
         <div class="logo <?php if( $eventum_lite_show_hdrtpinfo == ''){ ?>hdrlogo<?php } ?>">
           <?php eventum_lite_the_custom_logo(); ?>
            <div class="site_branding">
                <h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo('name'); ?></a></h1>
                <?php $description = get_bloginfo( 'description', 'display' );
                if ( $description || is_customize_preview() ) : ?>
                    <p><?php echo esc_html($description); ?></p>
                <?php endif; ?>
            </div>
         </div><!-- logo --> 
         
          <div class="MenuPart_Right"> 
             <div id="navigationpanel"> 
                 <nav id="main-navigation" class="header-navigation" role="navigation" aria-label="Primary Menu">
                    <button type="button" class="menu-toggle">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <?php
                    	wp_nav_menu( array(
                        'theme_location' => 'primary',
                        'menu_id'        => 'primary-menu',
                        'menu_class'     => 'nav-menu',
                    ) );
                    ?>
                </nav><!-- #main-navigation -->  
            </div><!-- #navigationpanel -->   
             <?php
                $eventum_lite_appointmentbtn = get_theme_mod('eventum_lite_appointmentbtn');
                if( !empty($eventum_lite_appointmentbtn) ){ ?>        
                <?php $eventum_lite_appointmentbtnlink = get_theme_mod('eventum_lite_appointmentbtnlink');
                if( !empty($eventum_lite_appointmentbtnlink) ){ ?>              
                        <div class="appbox">
                        <a class="appontmentbtn" target="_blank" href="<?php echo esc_url($eventum_lite_appointmentbtnlink); ?>">
                        	<?php echo esc_html($eventum_lite_appointmentbtn); ?>            
                        </a>
                     </div>
             <?php }} ?>
          </div><!-- .MenuPart_Right --> 
          
          
         <div class="clear"></div>
           
       </div><!-- .container -->  
    </div><!-- .LogoNavi-Panel --> 
 <div class="clear"></div> 
</div><!--.site-header --> 
 
<?php 
if ( is_front_page() && !is_home() ) {
if($eventum_lite_hdrfrontslider_show != '') {
	for($i=1; $i<=3; $i++) {
	  if( get_theme_mod('eventum_lite_frontslide_pic'.$i,false)) {
		$slider_Arr[] = absint( get_theme_mod('eventum_lite_frontslide_pic'.$i,true));
	  }
	}
?> 
<div class="FrontSlider">              
<?php if(!empty($slider_Arr)){ ?>
<div id="slider" class="nivoSlider">
<?php 
$i=1;
$slidequery = new WP_Query( array( 'post_type' => 'page', 'post__in' => $slider_Arr, 'orderby' => 'post__in' ) );
while( $slidequery->have_posts() ) : $slidequery->the_post();
$image = wp_get_attachment_url( get_post_thumbnail_id($post->ID)); 
$thumbnail_id = get_post_thumbnail_id( $post->ID );
$alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true); 
?>
<?php if(!empty($image)){ ?>
<img src="<?php echo esc_url( $image ); ?>" title="#slidecaption<?php echo esc_attr( $i ); ?>" alt="<?php echo esc_attr($alt); ?>" />
<?php }else{ ?>
<img src="<?php echo esc_url( get_template_directory_uri() ) ; ?>/images/slides/slider-default.jpg" title="#slidecaption<?php echo esc_attr( $i ); ?>" alt="<?php echo esc_attr($alt); ?>" />
<?php } ?>
<?php $i++; endwhile; ?>
</div>   

<?php 
$j=1;
$slidequery->rewind_posts();
while( $slidequery->have_posts() ) : $slidequery->the_post(); ?>                 
    <div id="slidecaption<?php echo esc_attr( $j ); ?>" class="nivo-html-caption">         
     <h2><?php the_title(); ?></h2>
     <p><?php $excerpt = get_the_excerpt(); echo esc_html( eventum_lite_string_limit_words( $excerpt, esc_attr(get_theme_mod('eventum_lite_slide_excerpt_length','10')))); ?></p>
		<?php
        $eventum_lite_frontslide_pic_moretext = get_theme_mod('eventum_lite_frontslide_pic_moretext');
        if( !empty($eventum_lite_frontslide_pic_moretext) ){ ?>
            <a class="slidermorebtn" href="<?php the_permalink(); ?>"><?php echo esc_html($eventum_lite_frontslide_pic_moretext); ?></a>
        <?php } ?>  
                        
    </div>   
<?php $j++; 
endwhile;
wp_reset_postdata(); ?>   
<?php } ?>
</div><!-- .FrontSlider -->    
<?php } } ?> 

<?php if ( is_front_page() && ! is_home() ) { ?>   
   
   <section id="Section-1">
     <div class="container">              
   <?php if( $eventum_lite_twobx_settings_show != ''){ ?> 
       <div class="WelComeLeft"> 
         <div class="box-equal-height"> 
            <?php 
                for($n=1; $n<=4; $n++) {    
                if( get_theme_mod('eventum_lite_2colbx'.$n,false)) {      
                    $queryvar = new WP_Query('page_id='.absint(get_theme_mod('eventum_lite_2colbx'.$n,true)) );		
                    while( $queryvar->have_posts() ) : $queryvar->the_post(); ?>     
                     <div class="Column2BX <?php if($n % 2 == 0) { echo "last_column"; } ?>">  
                    	 <div class="topboxbg bgcolorbx<?php echo $n; ?>">
							  <?php if(has_post_thumbnail() ) { ?>
                                <div class="ThumBX">
                                  <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>                                
                                </div>        
                               <?php } ?>
                               <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                               <p><?php $excerpt = get_the_excerpt(); echo esc_html( eventum_lite_string_limit_words( $excerpt, esc_attr(get_theme_mod('eventum_lite_2colbx_excerpt_length','5')))); ?></p>
                        </div>
                      </div>
                    <?php endwhile;
                    wp_reset_postdata();                                  
                } } ?>                                 
               <div class="clear"></div>  
            </div>          
          </div>
         <?php } ?>    
          
         <?php if( $eventum_lite_show_welcomesection != ''){ ?>   
         <div class="WelComeRight">
           <?php 
                if( get_theme_mod('eventum_lite_welcomepage',false)) {      
                    $queryvar = new WP_Query('page_id='.absint(get_theme_mod('eventum_lite_welcomepage',true)) );		
                    while( $queryvar->have_posts() ) : $queryvar->the_post(); ?>    
                               <div class="welcome_contentBX">
								<?php
                                $eventum_lite_welcomesection_subtitle = get_theme_mod('eventum_lite_welcomesection_subtitle');
                                if( !empty($eventum_lite_welcomesection_subtitle) ){ ?>
                                	<h5><?php echo esc_html($eventum_lite_welcomesection_subtitle); ?></h5>
                                <?php } ?>
                               <h2><?php the_title(); ?></h2>
                               <p><?php $excerpt = get_the_excerpt(); echo esc_html( eventum_lite_string_limit_words( $excerpt, esc_attr(get_theme_mod('eventum_lite_excerpt_length_welcomepage','100')))); ?></p>
								<?php
                                $eventum_lite_welcome_readmoretext = get_theme_mod('eventum_lite_welcome_readmoretext');
                                if( !empty($eventum_lite_welcome_readmoretext) ){ ?>
                                <a class="ReadMoreBtn" href="<?php the_permalink(); ?>"><?php echo esc_html($eventum_lite_welcome_readmoretext); ?></a>
                                <?php } ?>
                         	 </div>
                    <?php endwhile;
                    wp_reset_postdata();                                  
                } ?> 
            </div>  
          <?php } ?>
          
        <div class="clear"></div>    
      </div><!-- .container -->
    </section><!-- #Section-1-->
<?php } ?>