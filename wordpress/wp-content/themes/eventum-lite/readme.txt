=== Eventum Lite ===
Contributors: gracethemes
Tags:blog,portfolio,two-columns,right-sidebar,full-width-template,custom-colors,custom-menu,
custom-header,custom-logo,featured-images,editor-style,custom-background,threaded-comments,
theme-options, translation-ready
Requires at least: 5.0
Requires PHP:  5.6
Tested up to: 6.0
License: GNU General Public License version 2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==
Eventum Lite is one of the best themes for event management companies, conference, events, exhibition, expo, meeting, meetup, schedule, seminar, speakers, event planners, wedding planners, or any other business dealing with the event planning niche. It's a fully responsive and retina-ready theme with a clean design and modern look. This free event WordPress theme has been designed to be easy to use for beginners and powerful for professionals. The theme also offers high customizability. A theme must be highly customizable so that every user can easily change the elements of the theme that appear to be unnecessary or not good. For this using a customizable theme is an essential thing to look for. This amazing free event WordPress theme offers its users super easy customization, which makes it an appropriate choice for website users.

== Theme License & Copyright == 

* Eventum Lite WordPress Theme, Copyright 2022 Grace Themes 
* Eventum Lite is distributed under the terms of the GNU GPL

== Changelog ==

= 1.0 =
* Initial version release

= 1.1 =
* link correction


== Resources ==

Theme is Built using the following resource bundles.

= jQuery Nivo Slider =
* Name of Author: Dev7studios
* Copyright: 2010-2012
* https://github.com/Codeinwp/Nivo-Slider-jQuery
* License: MIT License
* https://github.com/Codeinwp/Nivo-Slider-jQuery/blob/master/license.txt

= Customizer-Pro =
* Name of Author: Justin Tadlock
* Copyright 2016, Justin Tadlock justintadlock.com
* https://github.com/justintadlock/trt-customizer-pro
* License: GNU General Public License v2.0
* http://www.gnu.org/licenses/gpl-2.0.html

= Images =
Image for theme screenshot, Copyright maxpixel.net
License: CC0 1.0 Universal (CC0 1.0)
Source: https://www.maxpixel.net/Meeting-Office-Group-Team-Teamwork-Business-5395567 (slider image)

= Images =
Image for theme screenshot, Copyright maxpixel.net
License: CC0 1.0 Universal (CC0 1.0)
Source: https://www.maxpixel.net/Icon-Crown-Transparent-Cutout-Royalty-Line-Art-7225224 (services icon)


= Font Awesome =
* https://fontawesome.com/icons

# Icons: CC BY 4.0 License (https://creativecommons.org/licenses/by/4.0/)
	In the Font Awesome Free download, the CC BY 4.0 license applies to all icons packaged as SVG and JS file types.

# Fonts: SIL OFL 1.1 License (https://scripts.sil.org/OFL)
	In the Font Awesome Free download, the SIL OFL license applies to all icons packaged as web and desktop font files.

# Code: MIT License (https://opensource.org/licenses/MIT)
	In the Font Awesome Free download, the MIT license applies to all non-font and non-icon files.

       
For any help you can mail us at support@gracethemes.com