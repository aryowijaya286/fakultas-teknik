<?php    
/**
 *eventum-lite Theme Customizer
 *
 * @package Eventum Lite
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function eventum_lite_customize_register( $wp_customize ) {	
	
	function eventum_lite_sanitize_dropdown_pages( $page_id, $setting ) {
	  // Ensure $input is an absolute integer.
	  $page_id = absint( $page_id );	
	  // If $page_id is an ID of a published page, return it; otherwise, return the default.
	  return ( 'publish' == get_post_status( $page_id ) ? $page_id : $setting->default );
	}

	function eventum_lite_sanitize_checkbox( $checked ) {
		// Boolean check.
		return ( ( isset( $checked ) && true == $checked ) ? true : false );
	} 
	
	function eventum_lite_sanitize_phone_number( $phone ) {
		// sanitize phone
		return preg_replace( '/[^\d+]/', '', $phone );
	} 
	
	
	function eventum_lite_sanitize_excerptrange( $number, $setting ) {	
		// Ensure input is an absolute integer.
		$number = absint( $number );	
		// Get the input attributes associated with the setting.
		$atts = $setting->manager->get_control( $setting->id )->input_attrs;	
		// Get minimum number in the range.
		$min = ( isset( $atts['min'] ) ? $atts['min'] : $number );	
		// Get maximum number in the range.
		$max = ( isset( $atts['max'] ) ? $atts['max'] : $number );	
		// Get step.
		$step = ( isset( $atts['step'] ) ? $atts['step'] : 1 );	
		// If the number is within the valid range, return it; otherwise, return the default
		return ( $min <= $number && $number <= $max && is_int( $number / $step ) ? $number : $setting->default );
	}

	function eventum_lite_sanitize_number_absint( $number, $setting ) {
		// Ensure $number is an absolute integer (whole number, zero or greater).
		$number = absint( $number );		
		// If the input is an absolute integer, return it; otherwise, return the default
		return ( $number ? $number : $setting->default );
	}
	
	// Ensure is an absolute integer
	function eventum_lite_sanitize_choices( $input, $setting ) {
		global $wp_customize; 
		$control = $wp_customize->get_control( $setting->id ); 
		if ( array_key_exists( $input, $control->choices ) ) {
			return $input;
		} else {
			return $setting->default;
		}
	}
	
		
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	
	$wp_customize->selective_refresh->add_partial( 'blogname', array(
		'selector' => '.logo h1 a',
		'render_callback' => 'eventum_lite_customize_partial_blogname',
	) );
	$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
		'selector' => '.logo p',
		'render_callback' => 'eventum_lite_customize_partial_blogdescription',
	) );
		
	 	
	//Panel for section & control
	$wp_customize->add_panel( 'eventum_lite_panel_settings', array(
		'priority' => 4,
		'capability' => 'edit_theme_options',
		'theme_supports' => '',
		'title' => __( 'Theme Settings', 'eventum-lite' ),		
	) );

	$wp_customize->add_section('eventum_lite_sitelayout',array(
		'title' => __('Layout Style','eventum-lite'),			
		'priority' => 1,
		'panel' => 	'eventum_lite_panel_settings',          
	));		
	
	$wp_customize->add_setting('eventum_lite_layoutoption',array(
		'sanitize_callback' => 'eventum_lite_sanitize_checkbox',
	));	 

	$wp_customize->add_control( 'eventum_lite_layoutoption', array(
    	'section'   => 'eventum_lite_sitelayout',    	 
		'label' => __('Check to Show Box Layout','eventum-lite'),
		'description' => __('check for box layout','eventum-lite'),
    	'type'      => 'checkbox'
     )); //Layout Settings 
	
	$wp_customize->add_setting('eventum_lite_colorscheme',array(
		'default' => '#24c373',
		'sanitize_callback' => 'sanitize_hex_color'
	));
	
	$wp_customize->add_control(
		new WP_Customize_Color_Control($wp_customize,'eventum_lite_colorscheme',array(
			'label' => __('Color Scheme','eventum-lite'),			
			'section' => 'colors',
			'settings' => 'eventum_lite_colorscheme'
		))
	);
	
	$wp_customize->add_setting('eventum_lite_secondcolor',array(
		'default' => '#fcb41e',
		'sanitize_callback' => 'sanitize_hex_color'
	));
	
	$wp_customize->add_control(
		new WP_Customize_Color_Control($wp_customize,'eventum_lite_secondcolor',array(
			'label' => __('Second Color','eventum-lite'),			
			'section' => 'colors',
			'settings' => 'eventum_lite_secondcolor'
		))
	);
	
	$wp_customize->add_setting('eventum_lite_hdrmenu',array(
		'default' => '#333333',
		'sanitize_callback' => 'sanitize_hex_color'
	));
	
	$wp_customize->add_control(
		new WP_Customize_Color_Control($wp_customize,'eventum_lite_hdrmenu',array(
			'label' => __('Navigation font Color','eventum-lite'),			
			'section' => 'colors',
			'settings' => 'eventum_lite_hdrmenu'
		))
	);
	
	
	$wp_customize->add_setting('eventum_lite_hdrmenuactive',array(
		'default' => '#24c373',
		'sanitize_callback' => 'sanitize_hex_color'
	));
	
	$wp_customize->add_control(
		new WP_Customize_Color_Control($wp_customize,'eventum_lite_hdrmenuactive',array(
			'label' => __('Navigation Hover/Active Color','eventum-lite'),			
			'section' => 'colors',
			'settings' => 'eventum_lite_hdrmenuactive'
		))
	);
	
	
	 //Header Contact Info
	$wp_customize->add_section('eventum_lite_hdrtpinfo',array(
		'title' => __('Header Contact Info','eventum-lite'),				
		'priority' => null,
		'panel' => 	'eventum_lite_panel_settings',
	));	
	
	$wp_customize->add_setting('eventum_lite_contactno',array(
		'default' => null,
		'sanitize_callback' => 'eventum_lite_sanitize_phone_number'	
	));
	
	$wp_customize->add_control('eventum_lite_contactno',array(	
		'type' => 'text',
		'label' => __('Enter phone number here','eventum-lite'),
		'section' => 'eventum_lite_hdrtpinfo',
		'setting' => 'eventum_lite_contactno'
	));
	
	$wp_customize->add_setting('eventum_lite_emailinfo',array(
		'sanitize_callback' => 'sanitize_email'
	));
	
	$wp_customize->add_control('eventum_lite_emailinfo',array(
		'type' => 'email',
		'label' => __('enter email id here.','eventum-lite'),
		'section' => 'eventum_lite_hdrtpinfo'
	));	
	
	
	$wp_customize->add_setting('eventum_lite_facebooklink',array(
		'default' => null,
		'sanitize_callback' => 'esc_url_raw'	
	));
	
	$wp_customize->add_control('eventum_lite_facebooklink',array(
		'label' => __('Add facebook link here','eventum-lite'),
		'section' => 'eventum_lite_hdrtpinfo',
		'setting' => 'eventum_lite_facebooklink'
	));	
	
	$wp_customize->add_setting('eventum_lite_twitterlink',array(
		'default' => null,
		'sanitize_callback' => 'esc_url_raw'
	));
	
	$wp_customize->add_control('eventum_lite_twitterlink',array(
		'label' => __('Add twitter link here','eventum-lite'),
		'section' => 'eventum_lite_hdrtpinfo',
		'setting' => 'eventum_lite_twitterlink'
	));

	
	$wp_customize->add_setting('eventum_lite_linkedinlink',array(
		'default' => null,
		'sanitize_callback' => 'esc_url_raw'
	));
	
	$wp_customize->add_control('eventum_lite_linkedinlink',array(
		'label' => __('Add linkedin link here','eventum-lite'),
		'section' => 'eventum_lite_hdrtpinfo',
		'setting' => 'eventum_lite_linkedinlink'
	));
	
	$wp_customize->add_setting('eventum_lite_instagramlink',array(
		'default' => null,
		'sanitize_callback' => 'esc_url_raw'
	));
	
	$wp_customize->add_control('eventum_lite_instagramlink',array(
		'label' => __('Add instagram link here','eventum-lite'),
		'section' => 'eventum_lite_hdrtpinfo',
		'setting' => 'eventum_lite_instagramlink'
	));		
	
	$wp_customize->add_setting('eventum_lite_show_hdrtpinfo',array(
		'default' => false,
		'sanitize_callback' => 'eventum_lite_sanitize_checkbox',
		'capability' => 'edit_theme_options',
	));	 
	
	$wp_customize->add_control( 'eventum_lite_show_hdrtpinfo', array(
	   'settings' => 'eventum_lite_show_hdrtpinfo',
	   'section'   => 'eventum_lite_hdrtpinfo',
	   'label'     => __('Check To show Header contact Section','eventum-lite'),
	   'type'      => 'checkbox'
	 ));//Show Contact Info
	 
	 $wp_customize->add_setting('eventum_lite_appointmentbtn',array(
		'default' => null,
		'sanitize_callback' => 'sanitize_text_field'	
	));
	
	$wp_customize->add_control('eventum_lite_appointmentbtn',array(	
		'type' => 'text',
		'label' => __('Enter button name here','eventum-lite'),
		'setting' => 'eventum_lite_bookbtn',
		'section' => 'eventum_lite_hdrtpinfo'
	));	
	
	$wp_customize->add_setting('eventum_lite_appointmentbtnlink',array(
		'default' => null,
		'sanitize_callback' => 'esc_url_raw'
	));
	
	$wp_customize->add_control('eventum_lite_appointmentbtnlink',array(
		'label' => __('Add button link here','eventum-lite'),		
		'setting' => 'eventum_lite_appointmentbtnlink',
		'section' => 'eventum_lite_hdrtpinfo'
	));	
	 
	 	
	//HomePage Slide Section		
	$wp_customize->add_section( 'eventum_lite_hdrfrontslider', array(
		'title' => __('Header Slider Settings', 'eventum-lite'),
		'priority' => null,
		'description' => __('Default image size for slider is 1400 x 610 pixel.','eventum-lite'), 
		'panel' => 	'eventum_lite_panel_settings',           			
    ));
	
	$wp_customize->add_setting('eventum_lite_frontslide_pic1',array(
		'default' => '0',			
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'eventum_lite_sanitize_dropdown_pages'
	));
	
	$wp_customize->add_control('eventum_lite_frontslide_pic1',array(
		'type' => 'dropdown-pages',
		'label' => __('Select page for slide 1:','eventum-lite'),
		'section' => 'eventum_lite_hdrfrontslider'
	));	
	
	$wp_customize->add_setting('eventum_lite_frontslide_pic2',array(
		'default' => '0',			
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'eventum_lite_sanitize_dropdown_pages'
	));
	
	$wp_customize->add_control('eventum_lite_frontslide_pic2',array(
		'type' => 'dropdown-pages',
		'label' => __('Select page for slide 2:','eventum-lite'),
		'section' => 'eventum_lite_hdrfrontslider'
	));	
	
	$wp_customize->add_setting('eventum_lite_frontslide_pic3',array(
		'default' => '0',			
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'eventum_lite_sanitize_dropdown_pages'
	));
	
	$wp_customize->add_control('eventum_lite_frontslide_pic3',array(
		'type' => 'dropdown-pages',
		'label' => __('Select page for slide 3:','eventum-lite'),
		'section' => 'eventum_lite_hdrfrontslider'
	));	//frontpage Slider Section	
	
	//Slider Excerpt Length
	$wp_customize->add_setting( 'eventum_lite_slide_excerpt_length', array(
		'default'              => 10,
		'type'                 => 'theme_mod',		
		'sanitize_callback'    => 'eventum_lite_sanitize_excerptrange',		
	) );
	$wp_customize->add_control( 'eventum_lite_slide_excerpt_length', array(
		'label'       => __( 'Slider Excerpt length','eventum-lite' ),
		'section'     => 'eventum_lite_hdrfrontslider',
		'type'        => 'range',
		'settings'    => 'eventum_lite_slide_excerpt_length','input_attrs' => array(
			'step'             => 1,
			'min'              => 0,
			'max'              => 50,
		),
	) );	
	
	$wp_customize->add_setting('eventum_lite_frontslide_pic_moretext',array(
		'default' => null,
		'sanitize_callback' => 'sanitize_text_field'	
	));
	
	$wp_customize->add_control('eventum_lite_frontslide_pic_moretext',array(	
		'type' => 'text',
		'label' => __('enter button name here','eventum-lite'),
		'section' => 'eventum_lite_hdrfrontslider',
		'setting' => 'eventum_lite_frontslide_pic_moretext'
	)); // slider read more button text
	
		
	$wp_customize->add_setting('eventum_lite_hdrfrontslider_show',array(
		'default' => false,
		'sanitize_callback' => 'eventum_lite_sanitize_checkbox',
		'capability' => 'edit_theme_options',
	));	 
	
	$wp_customize->add_control( 'eventum_lite_hdrfrontslider_show', array(
	    'settings' => 'eventum_lite_hdrfrontslider_show',
	    'section'   => 'eventum_lite_hdrfrontslider',
	    'label'     => __('Check To Show This Section','eventum-lite'),
	   'type'      => 'checkbox'
	 ));//Show Home Page Slider Sections	
	 
	 
	 //Two Column Sections
	$wp_customize->add_section('eventum_lite_twobx_settings', array(
		'title' => __('Two Column Services','eventum-lite'),
		'description' => __('Select pages from the dropdown for three column services','eventum-lite'),
		'priority' => null,
		'panel' => 	'eventum_lite_panel_settings',          
	));
		
	$wp_customize->add_setting('eventum_lite_2colbx1',array(
		'default' => '0',			
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'eventum_lite_sanitize_dropdown_pages'
	));
 
	$wp_customize->add_control(	'eventum_lite_2colbx1',array(
		'type' => 'dropdown-pages',			
		'section' => 'eventum_lite_twobx_settings',
	));		
	
	$wp_customize->add_setting('eventum_lite_2colbx2',array(
		'default' => '0',			
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'eventum_lite_sanitize_dropdown_pages'
	));
 
	$wp_customize->add_control(	'eventum_lite_2colbx2',array(
		'type' => 'dropdown-pages',			
		'section' => 'eventum_lite_twobx_settings',
	));
	
	$wp_customize->add_setting('eventum_lite_2colbx3',array(
		'default' => '0',			
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'eventum_lite_sanitize_dropdown_pages'
	));
 
	$wp_customize->add_control(	'eventum_lite_2colbx3',array(
		'type' => 'dropdown-pages',			
		'section' => 'eventum_lite_twobx_settings',
	));
	
	$wp_customize->add_setting('eventum_lite_2colbx4',array(
		'default' => '0',			
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'eventum_lite_sanitize_dropdown_pages'
	));
 
	$wp_customize->add_control(	'eventum_lite_2colbx4',array(
		'type' => 'dropdown-pages',			
		'section' => 'eventum_lite_twobx_settings',
	));
	
	$wp_customize->add_setting( 'eventum_lite_2colbx_excerpt_length', array(
		'default'              => 5,
		'type'                 => 'theme_mod',		
		'sanitize_callback'    => 'eventum_lite_sanitize_excerptrange',		
	) );
	$wp_customize->add_control( 'eventum_lite_2colbx_excerpt_length', array(
		'label'       => __( 'Circle box excerpt length','eventum-lite' ),
		'section'     => 'eventum_lite_twobx_settings',
		'type'        => 'range',
		'settings'    => 'eventum_lite_2colbx_excerpt_length','input_attrs' => array(
			'step'             => 1,
			'min'              => 0,
			'max'              => 50,
		),
	) );	
	
	
	$wp_customize->add_setting('eventum_lite_twobx_settings_show',array(
		'default' => false,
		'sanitize_callback' => 'eventum_lite_sanitize_checkbox',
		'capability' => 'edit_theme_options',
	));		
	
	$wp_customize->add_control( 'eventum_lite_twobx_settings_show', array(
	   'settings' => 'eventum_lite_twobx_settings_show',
	   'section'   => 'eventum_lite_twobx_settings',
	   'label'     => __('Check To Show This Section','eventum-lite'),
	   'type'      => 'checkbox'
	 ));//Show Four box sections
	 
	 
	 //Welcome Sections
	$wp_customize->add_section('eventum_lite_welcomesection', array(
		'title' => __('Welcome Sections','eventum-lite'),
		'description' => __('Select pages from the dropdown for four column sections','eventum-lite'),
		'priority' => null,
		'panel' => 	'eventum_lite_panel_settings',          
	));
		
	$wp_customize->add_setting('eventum_lite_welcomepage',array(
		'default' => '0',			
		'capability' => 'edit_theme_options',
		'sanitize_callback' => 'eventum_lite_sanitize_dropdown_pages'
	));
 
	$wp_customize->add_control(	'eventum_lite_welcomepage',array(
		'type' => 'dropdown-pages',			
		'section' => 'eventum_lite_welcomesection',
	));		
	
	
	
	$wp_customize->add_setting( 'eventum_lite_excerpt_length_welcomepage', array(
		'default'              => 100,
		'type'                 => 'theme_mod',		
		'sanitize_callback'    => 'eventum_lite_sanitize_excerptrange',		
	) );
	$wp_customize->add_control( 'eventum_lite_excerpt_length_welcomepage', array(
		'label'       => __( 'Circle box excerpt length','eventum-lite' ),
		'section'     => 'eventum_lite_welcomesection',
		'type'        => 'range',
		'settings'    => 'eventum_lite_excerpt_length_welcomepage','input_attrs' => array(
			'step'             => 1,
			'min'              => 0,
			'max'              => 50,
		),
	) );
	
	$wp_customize->add_setting('eventum_lite_welcome_readmoretext',array(
		'default' => null,
		'sanitize_callback' => 'sanitize_text_field'	
	));
	
	$wp_customize->add_control('eventum_lite_welcome_readmoretext',array(	
		'type' => 'text',
		'label' => __('Enter read more button text','eventum-lite'),
		'section' => 'eventum_lite_welcomesection',
		'setting' => 'eventum_lite_welcome_readmoretext'
	)); // slider read more button text	
	
	
	$wp_customize->add_setting('eventum_lite_show_welcomesection',array(
		'default' => false,
		'sanitize_callback' => 'eventum_lite_sanitize_checkbox',
		'capability' => 'edit_theme_options',
	));		
	
	$wp_customize->add_control( 'eventum_lite_show_welcomesection', array(
	   'settings' => 'eventum_lite_show_welcomesection',
	   'section'   => 'eventum_lite_welcomesection',
	   'label'     => __('Check To Show This Section','eventum-lite'),
	   'type'      => 'checkbox'
	 ));//Show Welcome sections
	
	 
	 //Blog Posts Settings
	$wp_customize->add_panel( 'eventum_lite_blogsettings_panel', array(
		'priority' => 3,
		'capability' => 'edit_theme_options',
		'theme_supports' => '',
		'title' => __( 'Blog Posts Settings', 'eventum-lite' ),		
	) );
	
	$wp_customize->add_section('eventum_lite_blogmeta_options',array(
		'title' => __('Blog Meta Options','eventum-lite'),			
		'priority' => null,
		'panel' => 	'eventum_lite_blogsettings_panel', 	         
	));		
	
	$wp_customize->add_setting('eventum_lite_hide_blogdate',array(
		'sanitize_callback' => 'eventum_lite_sanitize_checkbox',
	));	 

	$wp_customize->add_control( 'eventum_lite_hide_blogdate', array(
    	'label' => __('Check to hide post date','eventum-lite'),	
		'section'   => 'eventum_lite_blogmeta_options', 
		'setting' => 'eventum_lite_hide_blogdate',		
    	'type'      => 'checkbox'
     )); //Blog Post Date
	 
	 
	 $wp_customize->add_setting('eventum_lite_hide_postcats',array(
		'sanitize_callback' => 'eventum_lite_sanitize_checkbox',
	));	 

	$wp_customize->add_control( 'eventum_lite_hide_postcats', array(
		'label' => __('Check to hide post category','eventum-lite'),	
    	'section'   => 'eventum_lite_blogmeta_options',		
		'setting' => 'eventum_lite_hide_postcats',		
    	'type'      => 'checkbox'
     )); //blog Posts category	 
	 
	 
	 $wp_customize->add_section('eventum_lite_postfeatured_image',array(
		'title' => __('Posts Featured image','eventum-lite'),			
		'priority' => null,
		'panel' => 	'eventum_lite_blogsettings_panel', 	         
	));		
	
	$wp_customize->add_setting('eventum_lite_hide_postfeatured_image',array(
		'sanitize_callback' => 'eventum_lite_sanitize_checkbox',
	));	 

	$wp_customize->add_control( 'eventum_lite_hide_postfeatured_image', array(
		'label' => __('Check to hide post featured image','eventum-lite'),
    	'section'   => 'eventum_lite_postfeatured_image',		
		'setting' => 'eventum_lite_hide_postfeatured_image',	
    	'type'      => 'checkbox'
     )); //Posts featured image
	
	$wp_customize->add_section('eventum_lite_blogpost_content_settings',array(
		'title' => __('Posts Excerpt Options','eventum-lite'),			
		'priority' => null,
		'panel' => 	'eventum_lite_blogsettings_panel', 	         
	 ));	 
	 
	$wp_customize->add_setting( 'eventum_lite_blogexcerptrange', array(
		'default'              => 30,
		'type'                 => 'theme_mod',
		'transport' 		   => 'refresh',
		'sanitize_callback'    => 'eventum_lite_sanitize_excerptrange',		
	) );
	
	$wp_customize->add_control( 'eventum_lite_blogexcerptrange', array(
		'label'       => __( 'Excerpt length','eventum-lite' ),
		'section'     => 'eventum_lite_blogpost_content_settings',
		'type'        => 'range',
		'settings'    => 'eventum_lite_blogexcerptrange','input_attrs' => array(
			'step'             => 1,
			'min'              => 0,
			'max'              => 50,
		),
	) );

    $wp_customize->add_setting('eventum_lite_blogfullcontent',array(
        'default' => 'Excerpt',     
        'sanitize_callback' => 'eventum_lite_sanitize_choices'
	));
	
	$wp_customize->add_control('eventum_lite_blogfullcontent',array(
        'type' => 'select',
        'label' => __('Posts Content','eventum-lite'),
        'section' => 'eventum_lite_blogpost_content_settings',
        'choices' => array(
        	'Content' => __('Content','eventum-lite'),
            'Excerpt' => __('Excerpt','eventum-lite'),
            'No Content' => __('No Excerpt','eventum-lite')
        ),
	) ); 
	
	
	$wp_customize->add_section('eventum_lite_postsinglemeta',array(
		'title' => __('Posts Single Settings','eventum-lite'),			
		'priority' => null,
		'panel' => 	'eventum_lite_blogsettings_panel', 	         
	));	
	
	$wp_customize->add_setting('eventum_lite_hide_postdate_fromsingle',array(
		'sanitize_callback' => 'eventum_lite_sanitize_checkbox',
	));	 

	$wp_customize->add_control( 'eventum_lite_hide_postdate_fromsingle', array(
    	'label' => __('Check to hide post date from single','eventum-lite'),	
		'section'   => 'eventum_lite_postsinglemeta', 
		'setting' => 'eventum_lite_hide_postdate_fromsingle',		
    	'type'      => 'checkbox'
     )); //Hide Posts date from single
	 
	 
	 $wp_customize->add_setting('eventum_lite_hide_postcats_fromsingle',array(
		'sanitize_callback' => 'eventum_lite_sanitize_checkbox',
	));	 

	$wp_customize->add_control( 'eventum_lite_hide_postcats_fromsingle', array(
		'label' => __('Check to hide post category from single','eventum-lite'),	
    	'section'   => 'eventum_lite_postsinglemeta',		
		'setting' => 'eventum_lite_hide_postcats_fromsingle',		
    	'type'      => 'checkbox'
     )); //Hide blogposts category single
		 
}
add_action( 'customize_register', 'eventum_lite_customize_register' );

function eventum_lite_custom_css(){ 
?>
	<style type="text/css"> 					
        a,
        #sidebar ul li a:hover,
		#sidebar ol li a:hover,							
        .DefaultPostList h3 a:hover,
		.site-footer ul li a:hover, 
		.site-footer ul li.current_page_item a,				
        .postmeta a:hover,
        .button:hover,
		h2.services_title span,			
		.blog-postmeta a:hover,
		.blog-postmeta a:focus,
		blockquote::before	
            { color:<?php echo esc_html( get_theme_mod('eventum_lite_colorscheme','#24c373')); ?>;}					 
            
        .pagination ul li .current, .pagination ul li a:hover, 
        #commentform input#submit:hover,
		.hdrinfobar,
        .nivo-controlNav a.active,
		.sd-search input, .sd-top-bar-nav .sd-search input,			
		a.blogreadmore,
		a.appontmentbtn:hover,	
		a.ReadMoreBtn:hover,
		.copyrigh-wrapper:before,										
        #sidebar .search-form input.search-submit,				
        .wpcf7 input[type='submit'],				
        nav.pagination .page-numbers.current,		
		.morebutton,
		.nivo-directionNav a:hover,	
		.nivo-caption .slidermorebtn:hover		
            { background-color:<?php echo esc_html( get_theme_mod('eventum_lite_colorscheme','#24c373')); ?>;}
			

		
		.tagcloud a:hover,
		.logo::after,
		.logo,
		blockquote
            { border-color:<?php echo esc_html( get_theme_mod('eventum_lite_colorscheme','#24c373')); ?>;}
			
		#SiteWrapper a:focus,
		input[type="date"]:focus,
		input[type="search"]:focus,
		input[type="number"]:focus,
		input[type="tel"]:focus,
		input[type="button"]:focus,
		input[type="month"]:focus,
		button:focus,
		input[type="text"]:focus,
		input[type="email"]:focus,
		input[type="range"]:focus,		
		input[type="password"]:focus,
		input[type="datetime"]:focus,
		input[type="week"]:focus,
		input[type="submit"]:focus,
		input[type="datetime-local"]:focus,		
		input[type="url"]:focus,
		input[type="time"]:focus,
		input[type="reset"]:focus,
		input[type="color"]:focus,
		textarea:focus
            { outline:1px solid <?php echo esc_html( get_theme_mod('eventum_lite_colorscheme','#24c373')); ?>;}	
			
		a.ReadMoreBtn,
		a.appontmentbtn,
		.nivo-caption .slidermorebtn:hover 			
            { background-color:<?php echo esc_html( get_theme_mod('eventum_lite_secondcolor','#fcb41e')); ?>;}
			
		.site-footer h2::before,
		.site-footer h3::before,
		.site-footer h4::before,
		.site-footer h5::before
            { border-color:<?php echo esc_html( get_theme_mod('eventum_lite_secondcolor','#fcb41e')); ?>;}			
			
		
		.header-navigation a,
		.header-navigation ul li.current_page_parent ul.sub-menu li a,
		.header-navigation ul li.current_page_parent ul.sub-menu li.current_page_item ul.sub-menu li a,
		.header-navigation ul li.current-menu-ancestor ul.sub-menu li.current-menu-item ul.sub-menu li a  			
            { color:<?php echo esc_html( get_theme_mod('eventum_lite_hdrmenu','#333333')); ?>;}	
			
		
		.header-navigation ul.nav-menu .current_page_item > a,
		.header-navigation ul.nav-menu .current-menu-item > a,
		.header-navigation ul.nav-menu .current_page_ancestor > a,
		.header-navigation ul.nav-menu .current-menu-ancestor > a, 
		.header-navigation .nav-menu a:hover,
		.header-navigation .nav-menu a:focus,
		.header-navigation .nav-menu ul a:hover,
		.header-navigation .nav-menu ul a:focus,
		.header-navigation ul li a:hover, 
		.header-navigation ul li.current-menu-item a,			
		.header-navigation ul li.current_page_parent ul.sub-menu li.current-menu-item a,
		.header-navigation ul li.current_page_parent ul.sub-menu li a:hover,
		.header-navigation ul li.current-menu-item ul.sub-menu li a:hover,
		.header-navigation ul li.current-menu-ancestor ul.sub-menu li.current-menu-item ul.sub-menu li a:hover 		 			
            { color:<?php echo esc_html( get_theme_mod('eventum_lite_hdrmenuactive','#24c373')); ?>;}
			
		.hdrtopcart .cart-count
            { background-color:<?php echo esc_html( get_theme_mod('eventum_lite_hdrmenuactive','#24c373')); ?>;}		
			
		#SiteWrapper .header-navigation a:focus		 			
            { outline:1px solid <?php echo esc_html( get_theme_mod('eventum_lite_hdrmenuactive','#24c373')); ?>;}	
	
    </style> 
<?php                 
}
         
add_action('wp_head','eventum_lite_custom_css');	 

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function eventum_lite_customize_preview_js() {
	wp_enqueue_script( 'eventum_lite_customizer', get_template_directory_uri() . '/js/customize-preview.js', array( 'customize-preview' ), '19062019', true );
}
add_action( 'customize_preview_init', 'eventum_lite_customize_preview_js' );