<?php
/**
 * Eventum Lite About Theme
 *
 * @package Eventum Lite
 */

//about theme info
add_action( 'admin_menu', 'eventum_lite_abouttheme' );
function eventum_lite_abouttheme() {    	
	add_theme_page( __('About Theme Info', 'eventum-lite'), __('About Theme Info', 'eventum-lite'), 'edit_theme_options', 'eventum_lite_guide', 'eventum_lite_mostrar_guide');   
} 

//Info of the theme
function eventum_lite_mostrar_guide() { 	
?>

<h1><?php esc_html_e('About Theme Info', 'eventum-lite'); ?></h1>
<hr />  

<p><?php esc_html_e('Eventum Lite is one of the best themes for event management companies, conference, events, exhibition, expo, meeting, meetup, schedule, seminar, speakers, event planners, wedding planners, or any other business dealing with the event planning niche. It is a fully responsive and retina-ready theme with a clean design and modern look. This free event WordPress theme has been designed to be easy to use for beginners and powerful for professionals. The theme also offers high customizability. A theme must be highly customizable so that every user can easily change the elements of the theme that appear to be unnecessary or not good. For this using a customizable theme is an essential thing to look for. This amazing free event WordPress theme offers its users super easy customization, which makes it an appropriate choice for website users.', 'eventum-lite'); ?></p>

<h2><?php esc_html_e('Theme Features', 'eventum-lite'); ?></h2>
<hr />  
 
<h3><?php esc_html_e('Theme Customizer', 'eventum-lite'); ?></h3>
<p><?php esc_html_e('The built-in customizer panel quickly change aspects of the design and display changes live before saving them.', 'eventum-lite'); ?></p>


<h3><?php esc_html_e('Responsive Ready', 'eventum-lite'); ?></h3>
<p><?php esc_html_e('The themes layout will automatically adjust and fit on any screen resolution and looks great on any device. Fully optimized for iPhone and iPad.', 'eventum-lite'); ?></p>


<h3><?php esc_html_e('Cross Browser Compatible', 'eventum-lite'); ?></h3>
<p><?php esc_html_e('Our themes are tested in all mordern web browsers and compatible with the latest version including Chrome,Firefox, Safari, Opera, IE11 and above.', 'eventum-lite'); ?></p>


<h3><?php esc_html_e('E-commerce', 'eventum-lite'); ?></h3>
<p><?php esc_html_e('Fully compatible with WooCommerce plugin. Just install the plugin and turn your site into a full featured online shop and start selling products.', 'eventum-lite'); ?></p>

<hr />  	
<p><a href="http://www.gracethemesdemo.com/documentation/eventum/#homepage-lite" target="_blank"><?php esc_html_e('Documentation', 'eventum-lite'); ?></a></p>

<?php } ?>