<?php
namespace ElegantAddons\Widgets;

use ElegantAddons\Helper_Functions;
use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Control_Select;
use Elementor\Group_Control_Background;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Typography;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Elementor Hello World
 *
 * Elementor widget for hello world.
 *
 * @since 1.0.0
 */
class Eae_Switcher extends Widget_Base {

    /**
     * Retrieve the widget name.
     *
     * @since 1.0.0
     *
     * @access public
     *
     * @return string Widget name.
     */
    public function get_name() {
        return 'eae_switcher';
    }

    /**
     * Retrieve the widget title.
     *
     * @since 1.0.0
     *
     * @access public
     *
     * @return string Widget title.
     */

    public function get_title() {
        return __( 'EAE Switcher', 'elegant-addons-for-elementor' );
    }

    /**
     * Retrieve the widget icon.
     *
     * @since 1.0.0
     *
     * @access public
     *
     * @return string Widget icon.
     */
    public function get_icon() {
        return 'eicon-divider';
    }

    public function get_script_depends() {
        return [ 'eae-scripts' ];
    }

    public function get_style_depends() {
        return [
            'elegant-addons-css'
        ];
    }

     /**
     * Retrieve the list of categories the widget belongs to.
     *
     * Used to determine where to display the widget in the editor.
     *
     * Note that currently Elementor supports only one category.
     * When multiple categories passed, Elementor uses the first one.
     *
     * @since 1.0.0
     *
     * @access public
     *
     * @return array Widget categories.
     */
    public function get_categories() {
        return [ 'elegant-addons' ];
    }

    /**
     * Register the widget controls.
     *
     * Adds different input fields to allow the user to change and customize the widget settings.
     *
     * @since 1.0.0
     *
     * @access protected
     */
    protected function _register_controls() {
        $this->start_controls_section(
            'eae_switcher_content_section',
            [
                'label' => __( 'Content', 'elegant-addons-for-elementor' ),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'eae_switcher_title',
            [
                'label' => __( 'Title', 'elegant-addons-for-elementor' ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => __( 'List Title' , 'elegant-addons-for-elementor' ),
                'label_block' => true,
            ]
        );

        $repeater->add_control(
            'eae_switcher_source',
            [
                'label'   => esc_html__( 'Source', 'elegant-addons-for-elementor' ),
                'type'    => Controls_Manager::SELECT,
                'default' => 'custom',
                'options' => [
                    'custom'    => esc_html__( 'Custom', 'elegant-addons-for-elementor' ),
                    "template" => esc_html__( 'Template', 'elegant-addons-for-elementor' ),
                ],
            ]
        );

        $repeater->add_control(
            'eae_switcher_custom_template',
            [
                'label' => __( 'Custom', 'elegant-addons-for-elementor' ),
                'type' => \Elementor\Controls_Manager::SELECT,
                'default' => 'solid',
                'options' => eae_switcher_options(),
                'condition'   => [
                    'eae_switcher_source' => "template"
                ],
            ]
        );

        $repeater->add_control(
            'eae_switcher_content', [
                'label' => __( 'Content', 'elegant-addons-for-elementor' ),
                'type' => \Elementor\Controls_Manager::WYSIWYG,
                'default' => __( 'List Content' , 'elegant-addons-for-elementor' ),
                'show_label' => false,
            ]
        );

        $this->add_control(
            'eae_switcher_list',
            [
                'label' => __( 'Repeater List', 'elegant-addons-for-elementor' ),
                'type' => \Elementor\Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'default' => [
                    [
                        'eae_switcher_title' => __( 'Title #1', 'elegant-addons-for-elementor' ),
                        'eae_switcher_content' => __( 'Item content. Click the edit button to change this text.', 'elegant-addons-for-elementor' ),
                    ],
                    [
                        'eae_switcher_title' => __( 'Title #2', 'elegant-addons-for-elementor' ),
                        'eae_switcher_content' => __( 'Item content. Click the edit button to change this text.', 'elegant-addons-for-elementor' ),
                    ],
                ],
                'title_field' => '{{{ eae_switcher_title }}}',
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'eae_switcher_style',
            [
                'label' => __( 'Switcher Style', 'elegant-addons-for-elementor' ),
                'tab'   => Controls_Manager::TAB_STYLE,
            ]
        );
        $this->add_control(
            'eae_switcher_alignment',
            [
                'label' => __( 'Switcher Alignment', 'elegant-addons-for-elementor' ),
                'type' => \Elementor\Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => __( 'Left', 'elegant-addons-for-elementor' ),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __( 'Center', 'elegant-addons-for-elementor' ),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => __( 'Right', 'elegant-addons-for-elementor' ),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'default' => 'center',
                'toggle' => true,
                'selectors'  => [
                    '{{WRAPPER}} .eae-tab-container' => 'text-align: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'eae_switcher_background',
                'label' => __( 'Background', 'elegant-addons-for-elementor' ),
                'types' => [ 'classic', 'gradient' ],
                'selector' => '{{WRAPPER}} .eae-tab-container .eae-tab-toggler',
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name'        => 'eae_switcher__border',
                'placeholder' => '1px',
                'selector'    => '{{WRAPPER}} .eae-tab-container .eae-tab-toggler',
            ]
        );

        $this->add_responsive_control(
            'eae_switcher_padding',
            [
                'label'      => __( 'Padding', 'elegant-addons-for-elementor' ),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', 'em', '%' ],
                'selectors'  => [
                    '{{WRAPPER}} .eae-tab-container .eae-tab-toggler' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );
        $this->add_control(
            'eae_switcher_radius',
            [
                'label'      => __( 'Border Radius', 'elegant-addons-for-elementor' ),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors'  => [
                    '{{WRAPPER}} .eae-tab-container .eae-tab-toggler' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}; overflow: hidden;',
                ],
            ]
        );
        $this->add_control(
            'eae_switcher_content_spacing',
            [
                'label' => __( 'Content Spacing', 'elegant-addons-for-elementor' ),
                'type'  => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 100,
                    ],
                ],
                'default' => [
                    'size' => 20,
                ],
                'selectors' => [
                    '{{WRAPPER}} .eae-tab-item-content'    => 'margin-top: {{SIZE}}{{UNIT}}; margin-bottom: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();


        $this->start_controls_section(
            'eae_switcher_title_section',
            [
                'label' => __( 'Button Style', 'elegant-addons-for-elementor' ),
                'tab'   => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->start_controls_tabs( 'tabs_title_style' );

        $this->start_controls_tab(
            'tab_title_normal',
            [
                'label' => __( 'Normal', 'elegant-addons-for-elementor' ),
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name'      => 'eae_switcher_button_background',
                'types'     => [ 'classic', 'gradient' ],
                'selector'  => '{{WRAPPER}} .eae-tab-container .eae-tab-toggle-item',
                'separator' => 'after',
            ]
        );

        $this->add_control(
            'eae_switcher_button__color',
            [
                'label'     => __( 'Text Color', 'elegant-addons-for-elementor' ),
                'type'      => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .eae-tab-container .eae-tab-toggle-item' => 'color: {{VALUE}};',
                ],
                'separator' => 'before',
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name'     => 'eae_switcher_button_shadow',
                'selector' => '{{WRAPPER}} .eae-tab-container .eae-tab-toggle-item',
            ]
        );

        $this->add_responsive_control(
            'eae_switcher_button_padding',
            [
                'label'      => __( 'Padding', 'elegant-addons-for-elementor' ),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', 'em', '%' ],
                'selectors'  => [
                    '{{WRAPPER}} .eae-tab-container .eae-tab-toggle-item' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name'        => 'eae_switcher_button_border',
                'placeholder' => '1px',
                'default'     => '1px',
                'selector'    => '{{WRAPPER}} .eae-tab-container .eae-tab-toggle-item',
            ]
        );

        $this->add_control(
            'eae_switcher_button_radius',
            [
                'label'      => __( 'Border Radius', 'elegant-addons-for-elementor' ),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors'  => [
                    '{{WRAPPER}} .eae-tab-container .eae-tab-toggle-item' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}; overflow: hidden;',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'     => 'eae_switcher_button_typography',
                'selector' => '{{WRAPPER}} .eae-tab-container .eae-tab-toggle-item',
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'eae_switcher_button_tab_active',
            [
                'label' => __( 'Active', 'elegant-addons-for-elementor' ),
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name'      => 'eae_switcher_button_active_background',
                'types'     => [ 'classic', 'gradient' ],
                'selector'  => '{{WRAPPER}} .eae-tab-container .eae-tab-toggle-item.active',
                'separator' => 'after',
            ]
        );

        $this->add_control(
            'eae_switcher_button_active_color',
            [
                'label'     => __( 'Text Color', 'elegant-addons-for-elementor' ),
                'type'      => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .eae-tab-container .eae-tab-toggle-item.active' => 'color: {{VALUE}};',
                ],
                'separator' => 'before',
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name'     => 'eae_switcher_button_active_shadow',
                'selector' => '{{WRAPPER}} .eae-tab-container .eae-tab-toggle-item.active',
            ]
        );

        $this->add_control(
            'eae_switcher_button_active_border_color',
            [
                'label'     => __( 'Border Color', 'elegant-addons-for-elementor' ),
                'type'      => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .eae-tab-container .eae-tab-toggle-item.active' => 'border-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'eae_switcher_button_active_radius',
            [
                'label'      => __( 'Border Radius', 'elegant-addons-for-elementor' ),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors'  => [
                    '{{WRAPPER}} .eae-tab-container .eae-tab-toggle-item.active' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}; overflow: hidden;',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->end_controls_section();
        $this->start_controls_section(
            'eae_switcher_content_style_section',
            [
                'label' => __( 'Content Style', 'elegant-addons-for-elementor' ),
                'tab'   => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'eae_switcher_content_align',
            [
                'label'   => esc_html__( 'Alignment', 'elegant-addons-for-elementor' ),
                'type'    => Controls_Manager::CHOOSE,
                'default' => 'center',
                'options' => [
                    'left' => [
                        'title' => esc_html__( 'Left', 'elegant-addons-for-elementor' ),
                        'icon'  => 'fas fa-align-left',
                    ],
                    'center' => [
                        'title' => esc_html__( 'Center', 'elegant-addons-for-elementor' ),
                        'icon'  => 'fas fa-align-center',
                    ],
                    'right' => [
                        'title' => esc_html__( 'Right', 'elegant-addons-for-elementor' ),
                        'icon'  => 'fas fa-align-right',
                    ],
                    'justify' => [
                        'title' => esc_html__( 'Justified', 'elegant-addons-for-elementor' ),
                        'icon'  => 'fas fa-align-justify',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .eae-tab-item-content' => 'text-align: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'     => 'eae_switcher_content_typography',
                'selector' => '{{WRAPPER}} .eae-tab-item-content',
            ]
        );

        $this->end_controls_section();
    }

    /**
     * Render the widget output on the frontend.
     *
     * Written in PHP and used to generate the final HTML.
     *
     * @since 1.0.0
     *
     * @access protected
     */
    protected function render() {
        $settings = $this->get_settings_for_display();
        $id       = $this->get_id();
        $i        = 0;
        ?>

        <div class="eae-tab-container eae-left">
            <div class="eae-tab-toggler">
            <?php foreach (  $settings['eae_switcher_list'] as $item ) { ?>
                <?php
                    $active_class = '';
                    if ( $i == 0 ) {
                        $active_class = 'active';
                    }
                ?>
                <div class="eae-tab-toggle-item <?php echo esc_attr( $active_class ); ?>" data-tab="tab-<?php echo $item['_id']; ?>"><?php echo esc_html( $item['eae_switcher_title'] ); ?></div>
            <?php
                $i++;
                } //End Foreach
            ?>
            </div>
        </div>

        <div class="eae-tab-content">
            <?php foreach (  $settings['eae_switcher_list'] as $item ) { ?>
                <div class="eae-tab-item-content tab-<?php echo $item['_id']; ?>">
                    <?php
                        if ( $item['eae_switcher_source'] === 'custom' ) :
                            echo $this->parse_text_editor( $item['eae_switcher_content'] );
                        elseif ( $item['eae_switcher_source'] === 'template' && $item['eae_switcher_custom_template'] ) :
                            echo \Elementor\Plugin::instance()->frontend->get_builder_content_for_display( $item['eae_switcher_custom_template'] );
                        endif;
                    ?>
                </div>
            <?php } //End Foreach ?>
        </div>

        <?php
    }

}
