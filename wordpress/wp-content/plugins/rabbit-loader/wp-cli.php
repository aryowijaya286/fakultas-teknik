<?php

defined( 'ABSPATH' ) or die( 'No jokes please!' );

/**
* Activate RabbitLoader cloud services
*
* ## OPTIONS
*
* <ApiKey>
* : Your API key, can be obtained from profile page https://rabbitloader.com/account/#apikeys
*
* <ApiSecret>
* : Your API secret, can be obtained from profile page https://rabbitloader.com/account/#apikeys
*
* ## EXAMPLE
*
* wp rabbitloader connect ApiKey ApiSecret
*/

function cli_rabbitloader_connect($args, $assoc_args) {
    $apikey = !empty($args[0]) ? $args[0] : "";
    $apisecret = !empty($args[1]) ? $args[1] : "";
    $urlparts = parse_url(home_url());
    if(!empty($apikey) && !empty($apisecret)){
        RabbitLoader_21_Core::update_auth_keys($apikey, $apisecret, $urlparts['host'], '');
    }
}

WP_CLI::add_command("rabbitloader connect", "cli_rabbitloader_connect");
?>