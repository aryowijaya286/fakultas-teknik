=== RabbitLoader ===
Contributors: sanrl, abhishekgr
Tags: performance, critical css, defer css javascript, lazy load, page speed, cache, webp, cdn, image, core web vitals
Requires at least: 4.7
Tested up to: 6.1
Stable tag: 2.17.4
Requires PHP: 7.0
License: GNU General Public License, version 2
License URI: https://www.gnu.org/licenses/gpl-2.0.html

RabbitLoader can get up to 100/100 PageSpeed score for your website, boosting search ranking by making the Core Web Vitals (aka Chrome UX report) healthy in the long run by giving the best experience to your visitors.

== Description ==

Score 100/100 on Google PageSpeed Insights with RabbitLoader and achieve better SEO rankings. Load webpages within a few milliseconds to give your visitors the best end user experience and improve Core Web Vitals (CLS, TBT, TTI etc) which acts as an important search ranking signal.

No longer would you need different plugins for HTML JS and CSS minification, lazy loading of images and videos, image compression and WebP conversion, browser caching, and CDN to reduce overall latency. All these optimization plugins overwhelm your website and conflict with each other. 

Instead just use RabbitLoader for all your optimization needs for WordPress websites.

All the CPU intensive optimizations are performed by RabbitLoader cloud servers thus reducing usage of your hosting resources. This feature makes it possible for you to have a super-fast website without the need of an expensive host or a VPS server, just use any shared hosting environment or even a low end hosting configuration & still achieve 100 out of 100 PageSpeed Insights score.

[youtube https://www.youtube.com/watch?v=UjrRwkBXlkI]

What is RabbitLoader?

[RabbitLoader](https://rabbitloader.com/ "RabbitLoader") can help improving your website's loading time by using various techniques such as improving FCP, FID and CLS timings. As Page Speed is a very important ranking signal for Google, RabbitLoader helps you achieve higher rankings off Google.

*   No changes needed on your server or hosting
*   No technical knowledge required
*   Easy to setup

Benefits of using RabbitLoader:

*   [Score 100/100 in PageSpeed Insights](https://rabbitloader.com/best-pagespeed-score/ "Get 100/100 score in Google PageSpeed Insights Test")
*   Track PageSpeed Score for every URL of your website so you can quickly identify the pages that need additional improvements, this is very useful when you have hundreds and thousands of pages and checking all of them one by one manually could be difficult for you
*   [Load pages in Milliseconds](https://rabbitloader.com/load-site-faster/ "Load pages in Milliseconds") and reduce bounce rate, backed by world's best CDN service, we load all your assets from visitors nearest cache location, present in 215+ cities worldwide
*   RabbitLoader keeps your website up even if [the origin server is down](https://rabbitloader.com/origin-down/ "Secondary origin fallback")
*   Automatically convert Images to WebP format, [improves image loading](https://rabbitloader.com/articles/image-optimization/compressing-images-for-the-modern-web/ "") by 25-34% at equivalent SSIM quality index.
*   Load only critical CSS first to quickly render above the fold content and minimize render blocking parsing
*   Defer downloading and execution of JavaScript and non critical CSS files
*   Lazy loading of Images, YouTube/Vimeo videos, Google Maps or any iframe embedded content
*   Only below-the-fold images are lazy loaded, which keeps your LCP (Largest element above the fold) and CLS scores healthy
*   A LQIP/Blurred image placeholder is used when images are lazy loaded to give your visitors best experience, at the same time minimizing the layout reflow and boost CLS.
*   We use the latest content compression algorithm which minimizes the data transfer time noticeable by visitors on slow network 

Features (some of them are paid):

*   Comprehensive report to monitor SpeedScore of all URLs of your website even if you have thousands of pages
*   Convert and optimize images to WebP format on the fly, on an average reduces the image size by 25-34%.
*   Minification and compression of CSS, HTML and JavaScript code.
*   Static content including CSS, font files, images and JavaScript served via premium CDN servers
*   CDN servers available in 300+ locations (also known as PoP) worldwide, the best you can get
*   Extract critical CSS for every page automatically and defer non-critical CSS
*   Advanced resource loading mechanism to get best Chrome UX and PageSpeed score
*   Defer loading of heavy JS and non-critical CSS
*   Optimize web font loading and rendering
*   Lazy loading of images, videos, iframes
*   Use LQIP/Blurred image placeholder if lazy loading is on
*   Lazy loading of ads (AdSense, AdThrive etc)
*   Compress content (Gzip and Brotli) to improve data transfer speed over network
*   Option to exclude pages from being cached and full control through page rules
*   Woo-Commerce compatibility with multi currency plugins

== Frequently Asked Questions ==

= I am using WooCommerce, will RabbitLoader work? =

Yes, RabbitLoader works well for all content categories be it e-commerce, blog, or a fully dynamic page.

= Can I set page-rules for different sections of my website? =

Yes, if you want to customize the behavior, you can choose the correct content-type while [creating page rules](https://rabbitloader.com/kb/setting-page-rules/ "Setting up Page Rules").

= I am using Shared Hosting, will RabbitLoader work? =

Yes, RabbitLoader works well for shared, dedicated or VPS hosting irrespective of the hosting type and company except your local environment. Additional details available here if you want to [try the plugin on pre-production or staging environments](https://rabbitloader.com/kb/trying-staging-environment/ "Trying RabbitLoader on a staging environment"). 

= How does this plugin work? =

RabbitLoader makes sure that your Google PageSpeed Insights score is always 100 out of 100.
We improve FCP by removing render-blocking CSS and JS, reduce server response times, keep request counts low by deferring off screen payloads such as non-visible images.
RabbitLoader can help reducing the FID time by reducing the main thread work and allowing the browser to respond to user actions typically within 50 milliseconds.
We ensure the LCP element and styles needed are loaded as soon as possible and Render-blocking JS/CSS are deferred so LCP can render typically within 2.5 seconds.
Images are converted from JPEG/PNG to Next-Gen WebP format.
We try to minify and compress all possible assets to the best extent possible.

= Even after installing RabbitLoader plugin, my Google PageSpeed score is low. Why? =

After installing and activating RabbitLoader plugin, you need to connect it with our cloud service by clicking on "Connect with RabbitLoader" button on the 'Settings' tab of the plugin.

Once connected, it may take a few hours to optimize all pages of your website. The status are shown on the 'Home' tab of the RabbitLoader plugin page URL performance tab too.

If you are running ads program on your website, they can heavily impact the performance. Check [how to defer loading AdSense/AdThrive](https://rabbitloader.com/kb/lazy-loading-of-adsense-ads/ "Defer loading ads like AdSense/AdThrive to increase PageSpeed score") or other ad networks.

= Do I need a specific hardware configuration for my server? =

No. The plugin works equally efficient irrespective of the hardware size. All the heavy lifting optimization task is carried out outside your origin, adding no overhead to your CPU/RAM.

= Can I use it with other performance plugin? =

You can use other plugins along with RabbitLoader if their functionalities are not overlapping. Make sure you turn off those features which conflict.

= My hosting service uses Varnish cache? Can I use RabbitLoader with it? =

Yes, RabbitLoader is 100% compatible with Varnish cache and works great with it without any additional settings.

== Screenshots ==

1. Core features of RabbitLoader
2. Home tab of the plugin showing Lighthouse score for your pages based on Google PageSpeed.
3. RabbitLoader account overview page shows best and average Google PageSpeed score for all your pages, cached data usage etc.
4. Usage pattern and hits per day for cached and non-cached resources.
5. Connect your WordPress website with RabbitLoader account with one click.

== Changelog ==

= 2.17.x =
* Security and performance improvements

= 2.16.x =
* Compatibility improvement with other plugins

= 2.15 =
* Private mode for testing and debugging

= 2.14 =
* Control settings to discard cache

= 2.13 =
* More insights on CSS optimization

= 2.12 =
* More insights on image optimization

= 2.11 =
* Feature to ignore URL parameters

= 2.10 =
* Detail reports around CDN bandwidth usage and assets requests served

= 2.9 =
* Added URL performance report to easily identify good and bad performing URLs for site owners.

= 2.8 =
* Log messages to get more insights on things running under the hood, compatibility enhancements with LightSpeed server

= 2.7 =
* Enhancements in detecting new post and post status changes

= 2.3 =
* Added purge button on the post preview page

= 2.1 =
* Option to exclude URLs from optimization and caching

= 1.6 =
* Use of WordPress advance cache for faster response of cached data

= 1.1 =
* Automatic handling of optimization and caching
* Auto-Purge on website wide activities like theme change, etc

= 1.0 =
* First release of the plugin

== Upgrade Notice ==

= 1.0 =
Version 1 is the first release

By using the plugin, you agree to the RabbitLoader [Privacy Policy](https://rabbitloader.com/privacy/ "Privacy Policy") and [Terms](https://rabbitloader.com/terms/ "Terms").