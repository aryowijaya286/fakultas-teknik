<?php

class RabbitLoader_21_Util_Core{

    private static $request_uri = "";
    private static $request_url = "";
    private static $is_no_optimization = false;

    public static function get_param($name, $allowCase=false){
        $val = '';
        if(isset($_GET[$name])){
            $val = $_GET[$name];
        }else if(isset($_POST[$name])){
            $val = $_POST[$name];
        }

        if(!empty($val)){
            if($allowCase){
                $val = preg_replace( '/[^A-Za-z0-9_\-.]/', '', $val);
            }else{
                $val = sanitize_key($val);
            }
        }

        return $val;
    }
    public static function move(string $source, string $destination){
        if(rename($source, $destination)) {
            @unlink($source);
            return true;
        }else if(copy($source, $destination)) 
        {
            @unlink($source);
            return true;
        }
        return false;
    }
    /**
     * Same as file_put_contents with debug mode wrapper
     */
    public static function fpc($fp, &$data, $debug){
        $cache_dir = RabbitLoader_21_Util_WP::get_cache_dir('');
        if(!file_exists($cache_dir)){
            @mkdir($cache_dir, 0755, true);
        }
        if ($debug) {
            $file_updated = file_put_contents($fp, $data);
        } else {
            $file_updated = @file_put_contents($fp, $data);
        }
        return $file_updated;
    }

    /**
     * Same as file_append_contents with debug mode wrapper
     */
    public static function fac($fn, &$data, $debug){
        RabbitLoader_21_Core::get_log_file($fn, $fp);
        
        if ($debug) {
            $file_updated = file_put_contents($fp, $data, FILE_APPEND | LOCK_EX);
        } else {
            $file_updated = @file_put_contents($fp, $data, FILE_APPEND | LOCK_EX);
        }

        if(!$file_updated){
            //if file writing fails, ensure dir is created if missing
            $cache_dir = RabbitLoader_21_Util_WP::get_cache_dir('');
            if(!file_exists($cache_dir)){
                @mkdir($cache_dir, 0755, true);
            }
        }

        return $file_updated;
    }

    public static function get_request_type(){
        return empty($_SERVER['REQUEST_METHOD']) ? '' : strtolower($_SERVER['REQUEST_METHOD']);
    }

    private static function isHTTPS(){
        return (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == 443) || (isset($_SERVER['HTTPS']) && strcmp($_SERVER['HTTPS'], "off")!==0);
    }

    public static function isNoOptimization(){
        return self::$is_no_optimization || isset($_SERVER['HTTP_RL_NO_OPTIMIZATION']) || isset($_GET['rl-no-optimization']);
    }

    public static function reset_requested_uri(){
        self::$request_uri = '';
        self::$request_url = '';
    }

    public static function get_requested_uri(&$uri, &$url){
        if(!empty(self::$request_url)){
            $uri = self::$request_uri;
            $url = self::$request_url;
            return;
        }
        //process request
        if(isset($_SERVER['REQUEST_URI'])){
            $replace_count = 0;
            $_SERVER['REQUEST_URI'] = str_ireplace('rl-no-optimization=1','', $_SERVER['REQUEST_URI'], $replace_count);
            if($replace_count){
                $_SERVER['REQUEST_URI'] = rtrim($_SERVER['REQUEST_URI'], '?');
                self::$is_no_optimization = true;
            }
        }
        $request_uri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';

        $http_host = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '';
        $raw_link = (self::isHTTPS() ? "https" : "http") . "://$http_host$request_uri";

        $parsed_url = parse_url($raw_link);
        $query = empty($parsed_url['query']) ? '': trim($parsed_url['query']);
        if(!empty($query)){
            try{
                parse_str($query, $qs_vars);

                #remove default ignore params
                $ig_params = ['_gl', 'epik', 'fbclid', 'gbraid','gclid', 'msclkid', 'utm_source', 'utm_medium', 'utm_campaign','utm_content','utm_term','vgo_ee', 'wbraid', 'zenid' ,'rltest'];
                foreach($ig_params as $p){
                    unset($qs_vars[$p]);
                }

                #remove user defined ignore params
                RabbitLoader_21_Core::getWpUserOption($user_options);
                if(!empty($user_options['ignore_params'])){
                    $ignore_params = explode("\n", $user_options['ignore_params']);
                    if(!empty($ignore_params)){
                        foreach($ignore_params as $p){
                            unset($qs_vars[trim($p)]);
                        }
                    }
                }

                $query = http_build_query($qs_vars);
            }catch(Throwable $e){
                RabbitLoader_21_Core::on_exception($e);
            }
        }

        $uri = trim(@$parsed_url['path']).(empty($query)?'':'?'.$query);;
        $host = trim(@$parsed_url['host']);
        $scheme = trim(@$parsed_url['scheme']);
        $url = $scheme.'://'.$host.$uri;

        self::$request_uri = $uri;
        self::$request_url = $url;
    }

    public static function send_headers($headers){
        $headers_sent = 0;
        if(empty($headers)){
            return $headers_sent;
        }
        if(!is_array($headers)){
            $headers_decoded = json_decode($headers, true);
            if($headers_decoded===false){
                $e = new WP_Error( 'header_json_decode', json_last_error_msg(), $headers);
                RabbitLoader_21_Core::on_exception($e);
            }else{
                $headers = $headers_decoded;
            }
        }
        if(!empty($headers)){
            foreach($headers as $key=>$values){
                foreach($values as $val){
                    header($key.':'.$val, false);
                    $headers_sent++;
                } 
            }
        }
        return $headers_sent;
    }

    public static function isDev(){
        return ((defined('RABBITLOADER_PLUG_ENV') && RABBITLOADER_PLUG_ENV=='DEV') || (defined('RABBITLOADER_AC_PLUG_ENV') && RABBITLOADER_AC_PLUG_ENV=='DEV'));
    }
}

?>