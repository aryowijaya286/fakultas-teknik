<?php

class RabbitLoader_21_Core {

    private static $rl_wp_options = [];
    private static $user_options = [];

    /**
     * max time a cache can live
     */
    const ORPHANED_LONG_AGE_SEC = 30*24*3600;

    private static function addKeys(&$args, &$rabbitloader_field_domain){
        if(empty($args)){
            $args = [];
        }

        if(empty($args['headers'])){
            $args['headers'] = [];
        }

        if(RabbitLoader_21_Util_Core::isDev()){
            $args['sslverify'] = false;
        }
        $args['timeout'] = 30;

        $api_token = RabbitLoader_21_Core::getWpOptVal('api_token');
        if(!empty($api_token)){
            $args['headers'] += [
                'AUTHORIZATION'=>'Bearer '.$api_token
            ];
            $rabbitloader_field_domain = RabbitLoader_21_Core::getWpOptVal('domain');
        }else{

            $rabbitloader_field_apikey = get_option('rabbitloader_field_apikey');
            $rabbitloader_field_apisecret = get_option('rabbitloader_field_apisecret');
            $rabbitloader_field_domain = get_option('rabbitloader_field_domain');

            if(empty($rabbitloader_field_apikey) || empty($rabbitloader_field_apisecret)){
                return false;
            }
            
            $args['headers'] += [
                'APIKEY'=>$rabbitloader_field_apikey,
                'APISECRET'=>$rabbitloader_field_apisecret
            ];
        }
        
        return true;
    }   

    //@deprecated
    public static function update_auth_keys($key, $secret, $domain, $comments){
        update_option('rabbitloader_field_apikey', $key, false);
        update_option('rabbitloader_field_apisecret', $secret, false);
        update_option('rabbitloader_field_domain', $domain, false);
        update_option('rabbitloader_field_update_time', time(), false);
        if(!empty($comments)){
            update_option('rabbitloader_field_disconnect_reason', $comments, false);
        }
    }

    public static function update_api_tokens($api_token, $push_key, $domain, $did, $comments){
        RabbitLoader_21_Core::getWpOption($rl_wp_options);
        $rl_wp_options['api_token'] = $api_token;
        $rl_wp_options['push_key'] = empty($push_key) ? '' : wp_hash_password($push_key);
        $rl_wp_options['domain'] = $domain;
        $rl_wp_options['did'] = $did;
        $rl_wp_options['comments'] = $comments;
        $rl_wp_options['token_update_ts'] = time();
        RabbitLoader_21_Core::updateWpOption($rl_wp_options);
    }

    public static function getRLDomain(){
        return RabbitLoader_21_Util_Core::isDev() ? 'https://rabbitloader.local/' : 'https://rabbitloader.com/';
    }

    private static function isTemporaryError($apiMessage){
        $temp_errors = ['timed out', 'Could not resolve host', 'error setting certificate', 'Connection reset', 'OpenSSL', 'getaddrinfo() thread', 'SSL connection timeout', 'Unknown SSL', 'SSL_ERROR_SYSCALL', 'Failed connect to', 'cURL error 77'];
        $found = false;
        forEach($temp_errors as $msg){
            if(stripos($apiMessage, $msg)!==false){
                $found = true;
                break;
            }
        }
        return $found;
    }

    public static function &callGETAPI($endpoint, &$apiError, &$apiMessage){
        $http = [];
        $apiError = true;
        if(!RabbitLoader_21_Core::addKeys($args, $rabbitloader_field_domain)){
            $apiError = 'Keys could not be added';
            return $http;
        }
        $url = RabbitLoader_21_Core::getRLDomain().'api/v1/';
        if(strpos($endpoint, '?')){
            $endpoint.='&';
        }else{
            $endpoint.='?';
        }
        
        $endpoint.='domain='.$rabbitloader_field_domain.'&plugin_cms=wp&plugin_v='.RABBITLOADER_PLUG_VERSION.'&cms_v='.get_bloginfo( 'version' );

        $args['method'] = 'GET';

        try{
            $http = wp_remote_get( $url.$endpoint, $args);

            if(is_wp_error($http)){
                $apiError = true;
                $apiMessage = $http->get_error_message();
                if(empty($apiMessage)){$apiMessage='';}
                if(self::isTemporaryError($apiMessage)){
                    //chill, it happens
                }else{
                    RabbitLoader_21_Core::on_exception($http);
                }
                $http = [];
            }
            
            if(!empty($http['response']['code']) && in_array($http['response']['code'], [200, 401])){
                $http['body'] = json_decode($http['body'], true);
                if(!empty($http['body']['message'])){
                    $message = $http['body']['message'];
                    if(!strcmp($message, 'AUTH_REQUIRED') || !strcmp($message, 'INVALID_DOMAIN')){
                        RabbitLoader_21_Core::update_auth_keys('', '', '', "$message when $endpoint was called");
                        RabbitLoader_21_Core::update_api_tokens('', '', '', '', "$message when $endpoint was called");
                    }
                }
                $apiError = empty($http['body']['result']);
                $apiMessage = empty($http['body']['message']) ? '' : $http['body']['message'];
            }

        }catch(Throwable $e){
            RabbitLoader_21_Core::on_exception($e);
            $apiError = true;
            $apiMessage = $e->getMessage();
        }
        return $http;
    }

    public static function &callPostApi($endpoint, $body, &$apiError, &$apiMessage){
        $http = [];
        $apiError = true;

        if(!RabbitLoader_21_Core::addKeys($args, $rabbitloader_field_domain)){
            $apiError = 'Keys could not be added';
            return $http;
        }
        $url = RabbitLoader_21_Core::getRLDomain().'api/v1/';
        
        $body['domain'] = $rabbitloader_field_domain;
        $body['plugin_cms'] = 'wp';
        $body['plugin_v'] = RABBITLOADER_PLUG_VERSION;
        $body['cms_v'] = get_bloginfo( 'version' );

        $args['method'] = 'POST';
        $args['body'] = $body;

        try{
            $http = wp_remote_post( $url.$endpoint, $args);

            if(is_wp_error($http)){
                $apiError = true;
                $apiMessage = $http->get_error_message();
                if(empty($apiMessage)){$apiMessage='';}
                if(self::isTemporaryError($apiMessage)){
                    //chill, it happens
                }else{
                    RabbitLoader_21_Core::on_exception($http->get_error_message().$url.$endpoint);
                }
                $http = [];
            }

            if(!empty($http['response']['code']) && in_array($http['response']['code'], [200, 401])){
                $http['body'] = json_decode($http['body'], true);
                if(!empty($http['body']['message'])){
                    $message = $http['body']['message'];
                    if(!strcmp($message, 'AUTH_REQUIRED') || !strcmp($message, 'INVALID_DOMAIN')){
                        RabbitLoader_21_Core::update_auth_keys('', '', '', "$message when $endpoint was called");
                        RabbitLoader_21_Core::update_api_tokens('', '', '', '', "$message when $endpoint was called");
                    }
                }
                $apiError = empty($http['body']['result']);
                $apiMessage = empty($http['body']['message']) ? '' : $http['body']['message'];
            }
        }catch(Throwable $e){
            RabbitLoader_21_Core::on_exception($e);
            $apiError = true;
            $apiMessage = $e->getMessage();
        }
        return $http;
    }

    public static function get_cache_file_path($request_url, &$file){
        $file = RabbitLoader_21_Util_WP::get_cache_dir('long').DIRECTORY_SEPARATOR.md5($request_url);
    }
    
    public static function cache_exists_for_url($request_url, $post_mdfd_ts){
        RabbitLoader_21_Core::get_cache_file_path($request_url, $cache_file);
        return RabbitLoader_21_Core::cache_exists_for_hash($cache_file, $post_mdfd_ts);
    }
    public static function cache_exists_for_hash($cache_file, $post_mdfd_ts){
        $fn = $cache_file.'_c';
        $fe = file_exists($fn);
        if($post_mdfd_ts && $post_mdfd_ts>631152000){
            $mt = filemtime($fn);
            if($mt && $mt<$post_mdfd_ts){
                //post is modified after cache was generated
                $fe = false;
            }
        }
        return $fe;
    }
    
    public static function getWpUserOption(&$user_options){
        if(!empty(self::$user_options)){
            $user_options = self::$user_options;
            return;
        }
        if(function_exists('get_option')){
            $user_options = get_option('rabbit_loader_user_options');
        }else{
            RabbitLoader_21_Core::get_log_file('rl_user_options', $rl_user_options);
            if(file_exists($rl_user_options)){
                $user_options = json_decode(file_get_contents($rl_user_options), true);
            }
        }
        if(empty($user_options) || !is_array($user_options)){
            $user_options = [];
        }
        $default_values = [
            'purge_on_change'=>true,
            'exclude_patterns' => '',
            'ignore_params' => '',
            'private_mode_val'=>false,
        ];
        foreach($default_values as $k=>$v){
            if(!isset($user_options[$k])){
                $user_options[$k] = $v;
            }
        }
        self::$user_options = $user_options;
    }
    public static function updateUserOption(&$user_options){
        self::$user_options = $user_options;
        update_option('rabbit_loader_user_options', $user_options, true);
        try{
            RabbitLoader_21_Core::get_log_file('rl_user_options', $rl_user_options);
            $rl_json = json_encode($user_options);
            RabbitLoader_21_Util_Core::fpc($rl_user_options, $rl_json, WP_DEBUG);
        }catch(\Throwable $e){
            RabbitLoader_21_Core::on_exception($e);
        }
    }


    public static function getWpOption(&$rl_wp_options){
        if(!empty(self::$rl_wp_options)){
            $rl_wp_options = self::$rl_wp_options;
            return;
        }
        if(function_exists('get_option')){
            $rl_wp_options = get_option('rabbit_loader_wp_options');
        }else{
            RabbitLoader_21_Core::get_log_file('rl_config', $rl_config);
            if(file_exists($rl_config)){
                $rl_wp_options = json_decode(file_get_contents($rl_config), true);
            }
        }
        if(empty($rl_wp_options)){
            $rl_wp_options = [];
        }
        self::$rl_wp_options = $rl_wp_options;
    }
    /**
     * Get value of single config option
     */
    public static function getWpOptVal($key){
        RabbitLoader_21_Core::getWpOption($rl_wp_options);
        return isset($rl_wp_options[$key]) ? $rl_wp_options[$key] : '';
    }

    public static function updateWpOption(&$rl_wp_options){
        self::$rl_wp_options = $rl_wp_options;
        update_option('rabbit_loader_wp_options', $rl_wp_options, true);
        try{
            RabbitLoader_21_Core::get_log_file('rl_config', $rl_config);
            $rl_json = json_encode($rl_wp_options);
            RabbitLoader_21_Util_Core::fpc($rl_config, $rl_json, WP_DEBUG);
        }catch(\Throwable $e){
            RabbitLoader_21_Core::on_exception($e);
        }
    }

    public static function clean_orphaned_cached_files($orphanedFreqSec){
        if(empty($orphanedFreqSec)){
            $orphanedFreqSec = 300;
        }

        if(!function_exists('get_option') || !function_exists('update_option')){
            //may not be available if all WP files are not loaded
            return;
        }

        RabbitLoader_21_Core::getWpOption($rl_wp_options);
        if(empty($rl_wp_options)){
            $rl_wp_options = [
                'last_orphaned_cleanup'=>0,
                'rl_optimizer_engine_version'=>''
            ];
        }

        //version migrations start
        $user_options = [];
        RabbitLoader_21_Core::getWpUserOption($user_options);
        if(!empty($rl_wp_options['exclude_patterns']) && empty($user_options['exclude_patterns'])){
            //introduced@2.14.0
            $user_options['exclude_patterns'] = $rl_wp_options['exclude_patterns'];
            unset($rl_wp_options['exclude_patterns']);
            RabbitLoader_21_Core::updateUserOption($user_options);
        }
        if(!empty($rl_wp_options['ignore_params']) && empty($user_options['ignore_params'])){
            //introduced@2.14.0
            $user_options['ignore_params'] = $rl_wp_options['ignore_params'];
            unset($rl_wp_options['ignore_params']);
            RabbitLoader_21_Core::updateUserOption($user_options);
        }
        //version migrations end

        $prevRunSecAgo = PHP_INT_MAX;
        if(!empty($rl_wp_options['last_orphaned_cleanup'])){
            $prevRunSecAgo = time() - $rl_wp_options['last_orphaned_cleanup'];
            if($prevRunSecAgo < $orphanedFreqSec){
                #we have recently cleaned it within self::orphanedFreqSec seconds
                return;
            }
        }
        $rl_wp_options['last_orphaned_cleanup'] = time();
        RabbitLoader_21_Core::updateWpOption($rl_wp_options);

        $orphanedCleanTime = time() - RabbitLoader_21_Core::ORPHANED_LONG_AGE_SEC;
        $files = glob(RabbitLoader_21_Util_WP::get_cache_dir('long').'/*'); // get all file names
        $maxLimit = 500;//so we will not make the shutdown slow
        foreach($files as $file){ // iterate files
            if(is_file($file) && filemtime($file)<$orphanedCleanTime) {
                @unlink($file); // delete file
                --$maxLimit;
            }
            if(!$maxLimit){break;}
        }

        $anyPendingLog = false;
        $logs_to_send = [
            'cache_missed'=>2500,
            'error_log'=>5000
        ];
        $post_data = [];
        foreach($logs_to_send as $fn=>$length){
            RabbitLoader_21_Core::get_log_file($fn, $fp);
            if(file_exists($fp)){
                try{
                    $post_data[$fn] = file_get_contents($fp, false, null, 0, $length);
                    if(!empty($post_data[$fn])){
                        $anyPendingLog = true;
                    }
                    @unlink($fp);
                }catch(\Throwable $e){
                    $data = '';
                    RabbitLoader_21_Util_Core::fpc($fp, $data, false);
                }
            }
        }

        $hbeat_success_ts = empty($rl_wp_options['hbeat_success_ts'])?0:$rl_wp_options['hbeat_success_ts'];
        $hbeat_success_diff = time()-$hbeat_success_ts;

        if(!$anyPendingLog && $hbeat_success_diff <30*60){
            return;
        }

        $post_data['cdn_loop'] = empty($_SERVER['HTTP_CDN_LOOP']) ? '': $_SERVER['HTTP_CDN_LOOP'];
        if(empty($post_data['cdn_loop']) && !empty($_SERVER['HTTP_INCAP_CLIENT_IP'])){
            $post_data['cdn_loop'] = 'incap';
        }
        $post_data['server_addr'] = empty($_SERVER['SERVER_ADDR']) ? '': $_SERVER['SERVER_ADDR'];
        if(empty($post_data['server_addr']) && !empty($_SERVER['LOCAL_ADDR'])){
            $post_data['server_addr'] = $_SERVER['LOCAL_ADDR'];
        }

       $post_data['rl_plugin_instance'] = empty($rl_wp_options['rl_plugin_instance']) ? '': $rl_wp_options['rl_plugin_instance'];
        $post_data['rl_plugin_site_url'] = site_url();
        if(empty($post_data['rl_plugin_site_url'])){
            $post_data['rl_plugin_site_url'] = home_url();
        }
        $post_data['admin_ajax'] = admin_url( 'admin-ajax.php' );
        $http = RabbitLoader_21_Core::callPostApi('domain/heartbeat', $post_data, $apiError, $apiMessage);

        if(!$apiError && !empty($http['body']['data'])){
            $apiResponse = $http['body']['data'];

            $rl_wp_options['rabbitloader_field_apikey'] = get_option('rabbitloader_field_apikey');

            if(!empty($apiResponse['rl_plugin_instance'])){
                $rl_wp_options['rl_plugin_instance'] = $apiResponse['rl_plugin_instance'];
            }

            if(!empty($apiResponse['api_token'])){
                $rl_wp_options['api_token'] = $apiResponse['api_token'];
                $rl_wp_options['did'] = $apiResponse['did'];
                $rl_wp_options['push_key'] = wp_hash_password($apiResponse['push_key']);
            }

            if(!empty($apiResponse['rl_optimizer_engine_version'])){
                $server_version = $apiResponse['rl_optimizer_engine_version']; 
                if(empty($rl_wp_options['rl_optimizer_engine_version']) || $server_version != $rl_wp_options['rl_optimizer_engine_version']){
                    #we have update optimizer engine, and cache generated here was from previous engine. This can lead to performance issues.
                    RabbitLoader_21_Core::cleanAllCachedFiles('long');
                }
                $rl_wp_options['rl_optimizer_engine_version'] = $server_version;
            }

            if(!empty($apiResponse['rl_hb_messages'])){
                $rl_wp_options['rl_hb_messages'] = $apiResponse['rl_hb_messages'];
            }else{
                $rl_wp_options['rl_hb_messages'] = [];
            }

            if(!empty($apiResponse['rl_latest_plugin_v'])){
                $rl_wp_options['rl_latest_plugin_v'] = $apiResponse['rl_latest_plugin_v'];
            }

            if(empty($rl_wp_options['rl_varnish'])){
                $rl_wp_options['rl_varnish'] = self::check_varnish(2) ? 1 : -1;
            }
            $rl_wp_options['hbeat_success_ts'] = time();
        }
        RabbitLoader_21_Core::updateWpOption($rl_wp_options);
    }

    public static function cleanAllCachedFiles($cache_type){
        $deleted_count = 0;
        $files = glob(RabbitLoader_21_Util_WP::get_cache_dir($cache_type).'/*'); // get all file names
        foreach($files as $file){
            if(is_file($file)) {
                if(@unlink($file)){
                    ++$deleted_count;
                }
            }
        }
        return $deleted_count;
    }

    public static function purge_all(&$purge_count, $purge_source, &$tp_purge_count){
        try{
            //RL purges
            $purge_count = 0;
            $purge_count += RabbitLoader_21_Core::cleanAllCachedFiles('long');
            RabbitLoader_21_Core::callPostApi('purge/request', ['purge_source'=>$purge_source], $apiError, $apiMessage);

            //other common platforms purges
            RabbitLoader_21_TP::purge_all($tp_purge_count);
            
        }catch(Throwable $e){
            RabbitLoader_21_Core::on_exception($e);
        }
    }

    public static function purge_url_cache_local($url, &$local_purge_count, &$tp_purge_count){
        if(empty($url)){
            return;
        }
        $local_purge_count = 0;
        try{
            RabbitLoader_21_Core::get_cache_file_path($url, $cache_file);
            if(is_file($cache_file.'_c')) {
                if(@unlink($cache_file.'_c')){
                    ++$local_purge_count;
                } // delete file
            }
            RabbitLoader_21_TP::purge_url($url, $tp_purge_count);
        }catch(\Throwable $e){
            RabbitLoader_21_Core::on_exception($e);
        }
    }

    /**
     * @param string $fn file name
     * @param string $fp file path
     */
    public static function get_log_file($fn, &$fp){
        $fp = RabbitLoader_21_Util_WP::get_cache_dir().DIRECTORY_SEPARATOR.$fn.".log";
    }

    public static function on_exception($exception, $limit = 8){
        try{        
            $msg = "\n".date("c")." ";
            
            if(function_exists('is_wp_error') && is_wp_error($exception)){
                $msg .= $exception->get_error_message();
            }else if($exception instanceof Exception || $exception instanceof Throwable) {
                $msg .= $exception->getMessage();
            }else{
                $msg .= $exception;
            }
            if($limit>8){$limit=8;}
            $msg .= @print_r(debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, $limit),true);
            $msg .= @print_r($_SERVER,true);
          
            RabbitLoader_21_Util_Core::fac('error_log', $msg, WP_DEBUG);
            if(RabbitLoader_21_Util_Core::isDev()){
                echo $msg;
                error_log($msg);
            }
        }catch(Throwable $e){
            if(WP_DEBUG){
                echo $e->getMessage();
            }
        }
    }

    public static function sendJsonResponse(&$response){
        header("Content-Type: application/json");
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0, s-max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        $encoded_str = json_encode($response, JSON_INVALID_UTF8_IGNORE);
        if($encoded_str===false){
            echo '{"time":"1", "failed":"1"}';
        }else{
            echo $encoded_str;
        }
        exit;
    }

    public static function sendHeader(string $header, bool $replace=true){
        if(!headers_sent()){
            header($header, $replace);
        }
        /*else{
            $is_wp_cron_script = !empty($_SERVER['SCRIPT_NAME']) && stripos($_SERVER['SCRIPT_NAME'], '/wp-cron.php')!==false;
            $is_wp_cron_self = !empty($_SERVER['PHP_SELF']) && stripos($_SERVER['PHP_SELF'], '/wp-cron.php') !==false;
            $is_admin_ajax_self = !empty($_SERVER['PHP_SELF']) && stripos($_SERVER['PHP_SELF'], '/admin-ajax.php') !==false;
            $is_admin_update_self = !empty($_SERVER['PHP_SELF']) && stripos($_SERVER['PHP_SELF'], '/update.php') !==false;
            $is_wp_shell = !empty($_SERVER['SHELL']);
            if($is_wp_cron_script || $is_wp_cron_self || $is_wp_shell || $is_admin_ajax_self || $is_admin_update_self){
                //wp-cron will usually send the Cache-Control and Expires headers in advance, we need not to worry logging error in this case
                return;
            }
            
            RabbitLoader_21_Core::on_exception('Trying to send header when it is already sent. Header=> '.$header.'. Headers already sent=>'.print_r(headers_list(), true));
        }*/
    }

    private static function check_varnish($attempts){
        $httpcode = 0;
        try{
            $url_id = home_url().'/';
            $url_parts = parse_url($url_id);
            $port = (empty($url_parts['scheme']) || $url_parts['scheme']=='https') ? '443' : '80';
            $ch = curl_init($url_id);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PURGE");
            curl_setopt($ch, CURLOPT_RESOLVE, array($url_parts['host'].":$port:127.0.0.1"));
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_exec($ch);
            //$curl_error = curl_error($ch);
            $httpcode = intval(curl_getinfo($ch, CURLINFO_HTTP_CODE));
            curl_close($ch);
        }catch(Throwable $e){
            
        }
        if($httpcode==200){
            return true;
        }else if($attempts>0){
            $attempts--;
            if($attempts==0){return false;}
            return self::check_varnish($attempts);
        }
    }

    public static function get_common_cache_urls(&$urls_to_purge){
        if(empty($urls_to_purge)){
            $urls_to_purge = [];
        }

        $urls_to_purge[] = get_home_url(); //always purge home page if any other page is modified
        $urls_to_purge[] = get_home_url()."/"; //always purge home page if any other page is modified
        $urls_to_purge[] = home_url('/'); //always purge home page if any other page is modified
        $urls_to_purge[] = site_url('/'); //always purge home page if any other page is modified
        
        //clean pagination urls
        try{
            if(!empty(get_option('page_for_posts'))){
                $page_for_posts = get_permalink(get_option('page_for_posts'));
                if(is_string($page_for_posts) && !empty($page_for_posts) && get_option('show_on_front') == 'page'){
                    $urls_to_purge[] = $page_for_posts;
                }
            }
            
            $posts_per_page = get_option('posts_per_page');
            $published_posts = RabbitLoader_21_Core::get_published_count();
            $page_number_max = min(3, ceil($published_posts / $posts_per_page));
            for($pn=1; $pn<$page_number_max; $pn++){
                $urls_to_purge[] = home_url(sprintf('/page/%s/', $pn));
            }
        }catch(Throwable $e){
            RabbitLoader_21_Core::on_exception($e);
        }
    }

    public static function run_warmup($queued_offset, &$responses){
        try{
            RabbitLoader_21_Core::push_recent_posts($queued_offset, $queued_count, $published_count);
            $responses['queued_offset'] = $queued_offset;
            $responses['queued_count'] = $queued_count;
            $responses['published_count'] = $published_count;
            // Checking Hosting Name
            $hosting_name = RabbitLoader_21_Core::checkHostingName();
            if(!empty($hosting_name)){
                $responses['hosting_name'] = $hosting_name;
            }
            RabbitLoader_21_Core::clean_orphaned_cached_files(1);
        }catch(Throwable $e){
            $responses['exception'] = true;
            RabbitLoader_21_Core::on_exception($e);
        }
    }

    public static function get_published_count(){
        //$published_count = wp_count_posts()->publish + wp_count_posts('page')->publish;
        $published_count = 0;
        $post_types = get_post_types(['public' => true], 'names', 'and'); 
        foreach ( $post_types  as $post_type ) {
            $published_count += wp_count_posts($post_type)->publish;
        }
        return $published_count + 1;//1 for home page
    }

    public static function push_recent_posts(&$offset=0, &$queued_count=0, &$published_count=0){
        $permalinks = '';
        $posts_per_page = 250;
        $queued_count = 0;
        $latest_modified_ts = 0;

        //published posts
        $published_count = RabbitLoader_21_Core::get_published_count();

        $offset = intval($offset);
        if($offset>$published_count){
            $offset = 0;
        }

        $permalink_structure = get_option( 'permalink_structure' );
        $append_slash = substr($permalink_structure, -1) == "/" ? true : false;
        $args = array(
            'post_status' => 'publish',
            'post_type' => 'any',
            'orderby' => 'post_date',
            'order' => 'DESC',
            'fields' => 'ids', // Only get post IDs
            'posts_per_page' => $posts_per_page,
            'offset'=>intval($offset),
            'no_found_rows'=>false
        );

        try{
            $posts = get_posts($args);
            if(!empty($posts)){
                foreach($posts as $post_id){
                    $the_post = get_post( $post_id );
                    $permalink = get_permalink($the_post);
                    if(empty($permalink)){
                        continue;
                    }
                    if($append_slash){
                        $permalink = trailingslashit($permalink);
                    }else{
                        $permalink = $permalink.$append_slash;
                    }

                    $modified_ts = strtotime(get_the_modified_date($the_post));
                    if($modified_ts > $latest_modified_ts){
                        $latest_modified_ts = $modified_ts;
                    }
                    if(!self::cache_exists_for_url($permalink, $modified_ts)){
                        $permalinks.=$permalink."\n";
                        ++$queued_count;
                    }
                }
            }else{
                $offset = 0;
            }
        }catch(Throwable $e){
            $responses['exception'] = true;
            RabbitLoader_21_Core::on_exception($e);
        }

        //common URLs
        try{
            RabbitLoader_21_Core::get_common_cache_urls($urls_to_purge);
            if(!empty($urls_to_purge)){
                foreach($urls_to_purge as $url){
                    if(!self::cache_exists_for_url($url, $latest_modified_ts)){
                        $permalinks.=$url."\n";
                        ++$queued_count;
                    }
                }
            }
        }catch(\Throwable $e){
            RabbitLoader_21_Core::on_exception($e);
        }

        if(empty($permalinks)){
            return;
        }else{
            RabbitLoader_21_Util_Core::fac('cache_missed', $permalinks, WP_DEBUG);
        }
    }

    public static function run_diagnosis(&$responses){
        $constants = get_defined_constants(true);
        $constants = empty($constants['user']) ? [] : $constants['user'];
        //remove known sensitive info
        $sensitive_constants = ['DB_HOST', 'DB_NAME', 'DB_USER', 'DB_PASSWORD', 'AUTH_KEY', 'SECURE_AUTH_KEY', 'LOGGED_IN_KEY', 'NONCE_KEY', 'AUTH_SALT', 'SECURE_AUTH_SALT', 'LOGGED_IN_SALT', 'NONCE_SALT', 'COOKIEHASH', 'USER_COOKIE', 'PASS_COOKIE', 'AUTH_COOKIE', 'SECURE_AUTH_COOKIE', 'LOGGED_IN_COOKIE', 'RECOVERY_MODE_COOKIE'];
        foreach($sensitive_constants as $const_name){
            unset($constants[$const_name]);
        }
        $responses['server'] = $_SERVER;
        $responses['constants'] = $constants;
        $responses['classes'] = get_declared_classes();
        
        try{
            global $wpdb;
            $responses['options'] = $wpdb->get_results("select option_id, option_name from $wpdb->options");
        }catch(Throwable $e){
            RabbitLoader_21_Core::on_exception($e);
        }
    }

    private static function &checkHostingName(){
        $hosting_name = 'NA';
        if(!empty($_SERVER['cw_allowed_ip'])){
            $hosting_name  = $_SERVER['cw_allowed_ip'];
        }
        else if ( class_exists('WpeCommon') && method_exists( 'WpeCommon', 'purge_memcached' )) {
            $hosting_name = 'wpengine';
        }
        else if(defined("KINSTAMU_VERSION")){
            $hosting_name = 'Kinsta';
        }
        else if(defined("FLYWHEEL_PLUGIN_DIR")){
            $hosting_name = 'flywheel';
        }
        else if(preg_match("/^dp-.+/", gethostname())){
            $hosting_name = 'dreamhost';
        }
        else if(defined("CLOSTE_APP_ID")){
            $hosting_name = 'closte';
        }
        else if(function_exists( 'sg_cachepress_purge_cache')) {
            $hosting_name = 'siteground';
        }
        else if(class_exists('LiteSpeed_Cache_API') && method_exists('LiteSpeed_Cache_API', 'purge_all')) {
            $hosting_name = 'litespeed';
        }
        else if(class_exists('PagelyCachePurge') && method_exists('PagelyCachePurge','purgeAll')) {
            $hosting_name = 'pagely';
        }
        else if(class_exists('comet_cache') && method_exists('comet_cache', 'clear')) {
            $hosting_name = 'comet';
        }
        else if(defined('IS_PRESSABLE')) {
            $hosting_name = 'pressable';
        }
        return $hosting_name;
    }
}
?>