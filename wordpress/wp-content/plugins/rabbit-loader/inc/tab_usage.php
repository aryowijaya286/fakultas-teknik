<?php

if(class_exists('RabbitLoader_21_Tab_Usage')){
    #it seems we have a conflict
    return;
}

final class RabbitLoader_21_Tab_Usage extends RabbitLoader_21_Tab_Init{

    public static function init(){
        add_settings_section(
            'rabbitloader_section_usage',
            ' ',
            '',
            'rabbitloader-usage'
        );
        $start_date = date("Y-m-d", strtotime('-30 days'));
        $end_date = date("Y-m-d");
        $tbl = "RLUsageData.initV2(rabbitloader_local_vars.hostname, `".RabbitLoader_21_Core::getRLDomain()."`, `".RabbitLoader_21_Core::getWpOptVal('api_token')."`, `".$start_date."`, `".$end_date."`);";

        wp_enqueue_script('rabbitloader-luxon', RABBITLOADER_PLUG_URL . 'admin/js/luxon.min.v2.1.1.js', [], RABBITLOADER_PLUG_VERSION);
        wp_enqueue_script('rabbitloader-echarts', RABBITLOADER_PLUG_URL . 'admin/js/echarts.min.js', [], RABBITLOADER_PLUG_VERSION);
        wp_enqueue_script('rabbitloader-usage-js', RabbitLoader_21_Core::getRLDomain().'account/common/js/ec_area_stacked.js', ['rabbitloader-echarts', 'rabbitloader-luxon'], RABBITLOADER_PLUG_VERSION);
        wp_add_inline_script('rabbitloader-usage-js', $tbl);
    }

    public static function echoMainContent(){

        do_settings_sections( 'rabbitloader-usage' );
        $overview = self::getOverviewData($apiError, $apiMessage);

        ?>
        <div class="" style="max-width: 1160px; margin:40px auto;">
            <div class="row mb-4">
                <div class="col-sm-12 col-md-4">
                    <?php self::quota_used_box($overview, false);?>
                </div>
                <div class="col-sm-12 col-md-4">
                    <?php self::quota_remaining_box($overview);?>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="bg-white rounded p-4 tpopup" title="<?php RabbitLoader_21_Util_WP::_e(sprintf('Your usage cycle will be reset on %s',  date('jS M, Y', strtotime($overview['plan_end_date']))));?>">
                        <h4 class="">
                            <?php echo date('jS', strtotime($overview['plan_end_date']));?> <small style="font-size:14px;"><?php echo date('M', strtotime($overview['plan_end_date']));?></small>
                        </h4>
                        <span class="text-secondary mt-2"><?php RabbitLoader_21_Util_WP::_e('Next Reset');?></span>
                    </div>
                </div>
            </div>

            <div class="row mb-4">
                <div class="col">
                    <div class="bg-white rounded py-4">
                        <div class="row">
                            <div class="col-12 text-secondary">
                                <h5 class="px-4">CDN Usage</h5>
                                <span class="d-block px-4">Bandwidth usage pattern in last 30 days.</span>
                            </div>
                        </div>
                        <div class="p-4">
                            <div class="chart has-fixed-height" id="d3-area-stacked-nest-bandwidth" style="height:400px; width:100%;"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mb-4">
                <div class="col">
                    <div class="bg-white rounded py-4">
                        <div class="row">
                            <div class="col-12 text-secondary">
                                <h5 class="px-4">Assets Request</h5>
                                <span class="d-block px-4">Assets request pattern in last 30 days. This includes the number of times asset files such as CSS, JavaScript, Images, Font files etc were requested from CDN server nodes.</span>
                            </div>
                        </div>
                        <div class="p-4">
                            <div class="chart has-fixed-height" id="d3-area-stacked-nest-requests" style="height:400px; width:100%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}

?>