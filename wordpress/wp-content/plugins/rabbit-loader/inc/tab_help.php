<?php
if(class_exists('RabbitLoader_21_Tab_Help')){
    #it seems we have a conflict
    return;
}

class RabbitLoader_21_Tab_Help {

    public static function init(){
        add_settings_section(
            'rabbitloader_section_help',
            ' ',
            '',
            'rabbitloader-help'
        );
    }

    public static function echoMainContent(){

        do_settings_sections( 'rabbitloader-help' );
        ?>
        <div class="" style="max-width: 1160px; margin:40px auto;">
            <div class="row mb-4">
                <div class="col">
                    <div class="bg-white rounded p-4">
                        <div class="row">
                            
                            <div class="col-sm-12 col-md-8 text-secondary">
                                <h5 class="mt-0"><?php RabbitLoader_21_Util_WP::_e('Having trouble?');?></h5>
                                <span><?php RabbitLoader_21_Util_WP::_e('Facing issue with RabbitLoader plugin? Browse our knowledge base for common issues or reach out to our support team at support@rabbitloader.com');?></span>

                                <div class="mt-5">
                                    <a target="_blank" class="btn btn-primary mb-1 mb-sm-0" href="https://rabbitloader.com/kb/" ><?php RabbitLoader_21_Util_WP::_e('Browse Knowledge Base');?></a>
                                    <a target="_blank" class="btn btn-outline-primary" href="mailto:support@rabbitloader.com"><?php RabbitLoader_21_Util_WP::_e('Contact Support');?></a>
                                </div>

                            </div>
                            <div class="col-sm-12 col-md-4 text-center">
                                <img src="<?php echo RABBITLOADER_PLUG_URL;?>/assets/error.png" class="img-fluid" style="max-height:170px;"/> 
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col">
                    <div class="bg-white rounded p-4">
                        <div class="row"> 
                          <?php self::displayView(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      <?php
     }

    private static function &getKnowlegeBase(){
        $args = array('method' => 'GET', 'timeout' => 30);
        $res = wp_remote_request('https://rabbitloader.com/kb/wp-json/wp/v2/categories', $args);
        if (!is_wp_error($res) && ($res['response']['code'] == 200 || $res['response']['code'] == 201)) {
           $catData = json_decode($res['body'], true);
           $posts = self::getAllPost();
            $data = array(); 
            if (!empty($catData)) {
            foreach ($catData as $cat) {
               $list = array();
                if(!empty($posts)) {
                foreach ($posts as $post) {
                  if($post['categories'][0]==$cat['id']){
                    $list[] = array(
                        'title' => $post['title']['rendered'],
                        'link' => $post['link']
                    );
                  }
               }
             }
             $data[] = array(
                'id'=> $cat['id'],
                'category_name' => $cat['name'],
                'posts' => $list
               );
            }
        }
     }
     else {
          $data = "Something went wrong, Please try again later.";
     }
     return $data;
 }

    private static function &getAllPost(){
    $args = array( 
      'method' => 'GET',
      'timeout' => 30
    );
    $res = wp_remote_request("https://rabbitloader.com/kb/wp-json/wp/v2/posts?per_page=100", $args);
    //Check for success
    if(!is_wp_error($res) && ($res['response']['code'] == 200 || $res['response']['code'] == 201)) {
      $posts = json_decode($res['body'], true);
      return $posts;
    }
  }

   private static function displayView(){
      $posts = get_transient('rabbitloader_knowlegebase_data');
      if ($posts) {
          self::getRowData($posts);
         }else{
            $expiryInterval = 7*24*60*60; 
            $data = self::getKnowlegeBase();
            if(is_array($data) && !empty($data)){
              set_transient('rabbitloader_knowlegebase_data', $data, $expiryInterval);
              self::getRowData($data);
            }else{
              self::getRowData($data);
            }
            
        }
     }

     private static function getRowData($posts){
        if(is_array($posts)){
            foreach($posts as $post){
              echo '<div class="col-12 text-secondary">
                    <h5 class="mt-0">' . $post['category_name'] . '</h5>
                    <ul style="list-style:square;">';
                    foreach ($post['posts'] as $data) {
                     echo '<li><a class="text-secondary" href="' . $data['link'] . '" target="_blank" title="Read more" style="text-decoration:none;">' . $data['title']. '</a></li>';
                     }
                echo '</ul>
                  </div>';
                 }
              }
              else{
                echo '<div class="alert alert-danger text-center" role="alert"> Sorry, Colud not load knowledge base data. Please try again later.</div>';
              }

         }
   }
?>