<?php

class RabbitLoader_21_Util_WP{

    private static $isSearch = false;
    private static $isPageAccount = false;

    public static function init(){
        add_action( 'template_redirect', function(){
            self::$isSearch = (is_search() || !empty($_GET["s"]));
            self::$isPageAccount = (function_exists("is_page") && is_page("account"));
        });
    }
    
    public static function is_cli(){
        return (defined("WP_CLI") && WP_CLI);
    }

    public static function is_user_logged_in(){

        if(function_exists('is_user_logged_in')){
            //if WP is not initialized, we may not get the function, so we have to do our own checks as well
            return is_user_logged_in();
        }

        $cookies_keys = [];
        
        if(defined('RABBITLOADER_AC_LOGGED_IN_COOKIE')){
            $cookies_keys[] = RABBITLOADER_AC_LOGGED_IN_COOKIE;
        }

        if(defined('LOGGED_IN_COOKIE')){
            $cookies_keys[] = LOGGED_IN_COOKIE;
        }
        
        foreach ($cookies_keys as $key) {
            if (!empty($_COOKIE[$key])) {
                return true;
            }
        }
    
        return false;
    }

    public static function is_login_page()
    {
        $is_login = function_exists('is_login') && function_exists('wp_login_url') && is_login();

        $incl_path = str_replace(['\\', '/'], DIRECTORY_SEPARATOR, ABSPATH);

        return $is_login || (in_array($incl_path . 'wp-login.php', get_included_files())
            || in_array($incl_path . 'wp-register.php', get_included_files()))
            || (isset($_GLOBALS['pagenow']) && $GLOBALS['pagenow'] === 'wp-login.php')
            || $_SERVER['PHP_SELF'] == '/wp-login.php' || self::$isPageAccount;
    }

    public static function is_ajax(){

        $incl_path = str_replace(['\\', '/'], DIRECTORY_SEPARATOR, ABSPATH);

        return (function_exists("wp_doing_ajax") && wp_doing_ajax()) || 
        (defined('DOING_AJAX') && DOING_AJAX) || 
        (!empty($_SERVER["HTTP_X_REQUESTED_WITH"]) && $_SERVER["HTTP_X_REQUESTED_WITH"] == "XMLHttpRequest") ||
        (function_exists('get_included_files') && in_array($incl_path . 'wp-cron.php', get_included_files())) ||
        (function_exists('get_included_files') && in_array($incl_path . 'admin-ajax.php', get_included_files()));
    }

    public static function is_search():bool{
        return self::$isSearch;
    }

    /**
     * @return string directory path (without trailing slash) where cache files are stored
     */
    public static function &get_cache_dir($cache_type=''){
        $cache_dir = '';
        
        if(defined('RABBITLOADER_AC_CACHE_DIR')){
            $cache_dir = RABBITLOADER_AC_CACHE_DIR;
        }else if(defined('RABBITLOADER_CACHE_DIR')){
            $cache_dir = RABBITLOADER_CACHE_DIR;
        }else{
            $cache_dir = WP_CONTENT_DIR.DIRECTORY_SEPARATOR."rabbitloader";
        }
        if(!empty($cache_type)){
            $cache_dir = $cache_dir.DIRECTORY_SEPARATOR.$cache_type;
        }
        return $cache_dir;
    }

    public static function get_wp_config(){
        $wp_config_path = '';
        if(file_exists( ABSPATH . 'wp-config.php')) {
            $wp_config_path = ABSPATH . 'wp-config.php';
        } elseif(@file_exists(dirname( ABSPATH ).'/wp-config.php') && ! @file_exists( dirname( ABSPATH ) . '/wp-settings.php' ) ) {
            // config file is not part of another installation
            $wp_config_path = dirname( ABSPATH ) . '/wp-config.php';
        } else {
            $wp_config_path = false;
        }
        return $wp_config_path;
    }

    public static function _e($txt){
        echo RabbitLoader_21_Util_WP::__($txt);
    }
    public static function __($txt){
        return __($txt, RABBITLOADER_TEXT_DOMAIN);
    }
    public static function _n($txt_singular, $txt_plural, $count){
        return _n($txt_singular, $txt_plural, $count, RABBITLOADER_TEXT_DOMAIN);
    }
}

?>