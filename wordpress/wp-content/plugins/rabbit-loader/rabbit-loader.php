<?php

/**
 * Plugin Name: RabbitLoader
 * Plugin URI: https://rabbitloader.com
 * Author:       RabbitLoader
 * Author URI:   https://rabbitloader.com/
 * Description: RabbitLoader can improve Google PageSpeed score and get you 100 out of 100 by improving the page load time to just a few milliseconds. It improves the Core Web Vitals score for your pages and boost PageSpeed score to help better search rankings and best the experience for your end user.
 * Version: 2.17.4
 * Text Domain: rabbit-loader
 */
/*
RabbitLoader is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 2 of the License, or any later version. */

defined( 'ABSPATH' ) or die( 'ABSPATH not defined' );

$plug_dir = dirname(__FILE__) . '/';
include_once($plug_dir.'autoload.php');

if (is_admin() ) {

    register_activation_hook( __FILE__, 'RabbitLoader_21_Admin::activate_advanced_cache');
    register_deactivation_hook( __FILE__, 'RabbitLoader_21_Admin::plugin_deactivate');
    register_uninstall_hook(__FILE__, 'RabbitLoader_21_Admin::plugin_uninstall');
    add_filter( 'plugin_action_links_rabbit-loader/rabbit-loader.php', 'RabbitLoader_21_Admin::settings_link' );

    if(!defined('RABBITLOADER_UNINSTALL_MODE')){
        RabbitLoader_21_Admin::addActions();
    }
}else{
    if(!defined('RABBITLOADER_AC_ACTIVE')){
        //advance cache failed to work, as a fallback we can intercept requests here
        RabbitLoader_21_Public::process_incoming_request('fallback');
    }
    RabbitLoader_21_Public::addActions();
}

if(defined("WP_CLI") && WP_CLI){
    include_once($plug_dir.'wp-cli.php');
}
?>