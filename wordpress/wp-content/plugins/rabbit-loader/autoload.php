<?php

define('RABBITLOADER_PLUG_DIR', plugin_dir_path( __FILE__ ));
define('RABBITLOADER_CACHE_DIR', WP_CONTENT_DIR.DIRECTORY_SEPARATOR."rabbitloader");
define('RABBITLOADER_PLUG_URL', plugin_dir_url(__FILE__));
define('RABBITLOADER_PLUG_VERSION', '2.17.4');
define('RABBITLOADER_TEXT_DOMAIN', 'rabbit-loader');
define('RABBITLOADER_PLUG_ENV', 'PROD');

include_once( RABBITLOADER_PLUG_DIR.'inc/core/core.php');
include_once( RABBITLOADER_PLUG_DIR.'inc/core/util.php');
include_once( RABBITLOADER_PLUG_DIR.'inc/core/integrations.php');
include_once( RABBITLOADER_PLUG_DIR.'inc/rl_can_url.php');
include_once( RABBITLOADER_PLUG_DIR.'inc/public.php');
include_once( RABBITLOADER_PLUG_DIR.'inc/util_wp.php');

if (is_admin() ) {
    include_once( RABBITLOADER_PLUG_DIR.'inc/admin.php');
    include_once( RABBITLOADER_PLUG_DIR.'inc/conflicts.php');
    include_once( RABBITLOADER_PLUG_DIR.'inc/tab_init.php');
    include_once( RABBITLOADER_PLUG_DIR.'inc/tab_home.php');
    include_once( RABBITLOADER_PLUG_DIR.'inc/tab_urls.php');
    include_once( RABBITLOADER_PLUG_DIR.'inc/tab_usage.php');
    include_once( RABBITLOADER_PLUG_DIR.'inc/tab_geo.php');
    include_once( RABBITLOADER_PLUG_DIR.'inc/tab_help.php');
    include_once( RABBITLOADER_PLUG_DIR.'inc/tab_settings.php');
    include_once( RABBITLOADER_PLUG_DIR.'inc/tab_log.php');
    include_once( RABBITLOADER_PLUG_DIR.'inc/tab_images.php');
    include_once( RABBITLOADER_PLUG_DIR.'inc/tab_css.php');
}

RabbitLoader_21_Public::addActions();
add_action('wp_ajax_nopriv_rl_ep_callback', 'RabbitLoader_21_Public::setup_endpoint');