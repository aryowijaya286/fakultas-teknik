# Copyright (C) 2022 Kraft Plugins
# This file is distributed under the same license as the Wheel of Life package.
msgid ""
msgstr ""
"Project-Id-Version: Wheel of Life 1.1.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-11-28 09:52:34+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2022-MO-DA HO:MI+ZONE\n"
"Last-Translator: Kraft Plugins\n"
"Language-Team: \n"
"X-Poedit-KeywordsList: "
"__;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_"
"attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;\n"
"Language: en_US\n"
"X-Poedit-SearchPath-0: ../../wheel-of-life\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: grunt-wp-i18n 1.0.3\n"

#: includes/Wheel_Of_Life_Admin.php:196
msgid "Wheels of Life"
msgstr ""

#. Plugin Name of the plugin/theme
msgid "Wheel of Life"
msgstr ""

#: includes/Wheel_Of_Life_Admin.php:220
msgid "Wheel of Life - Global Settings"
msgstr ""

#: includes/Wheel_Of_Life_Admin.php:228
msgid "Wheel of Life - Submissions"
msgstr ""

#: includes/Wheel_Of_Life_Admin.php:258 includes/Wheel_Of_Life_Admin.php:259
msgid "Start Assessment"
msgstr ""

#: includes/Wheel_Of_Life_Admin.php:277
msgid "Enter Wheel Description..."
msgstr ""

#: includes/Wheel_Of_Life_PostTypes.php:67
msgid "Add New wheel"
msgstr ""

#: includes/Wheel_Of_Life_PostTypes.php:68
msgid "New wheel"
msgstr ""

#: includes/Wheel_Of_Life_PostTypes.php:69
msgid "Edit wheel"
msgstr ""

#: includes/Wheel_Of_Life_PostTypes.php:70
msgid "View wheel"
msgstr ""

#: includes/Wheel_Of_Life_PostTypes.php:71
msgid "All wheels"
msgstr ""

#: includes/Wheel_Of_Life_PostTypes.php:72
msgid "Search wheels"
msgstr ""

#: includes/Wheel_Of_Life_PostTypes.php:73
msgid "Parent wheels:"
msgstr ""

#: includes/Wheel_Of_Life_PostTypes.php:74
msgid "No wheels found."
msgstr ""

#: includes/Wheel_Of_Life_PostTypes.php:75
msgid "No wheel found in Trash."
msgstr ""

#: includes/Wheel_Of_Life_PostTypes.php:80
msgid "Description."
msgstr ""

#: includes/Wheel_Of_Life_PostTypes.php:197
msgid "Add New Submission"
msgstr ""

#: includes/Wheel_Of_Life_PostTypes.php:198
msgid "New Submission"
msgstr ""

#: includes/Wheel_Of_Life_PostTypes.php:199
msgid "Edit Submission"
msgstr ""

#: includes/Wheel_Of_Life_PostTypes.php:200
msgid "View Submission"
msgstr ""

#: includes/Wheel_Of_Life_PostTypes.php:201
msgid "All Submissions"
msgstr ""

#: includes/Wheel_Of_Life_PostTypes.php:202
msgid "Search Submissions"
msgstr ""

#: includes/Wheel_Of_Life_PostTypes.php:203
msgid "Parent Submissions:"
msgstr ""

#: includes/Wheel_Of_Life_PostTypes.php:204
msgid "No Submissions found."
msgstr ""

#: includes/Wheel_Of_Life_PostTypes.php:205
msgid "No Submission found in Trash."
msgstr ""

#: includes/Wheel_Of_Life_PostTypes.php:210
msgid "This is where wheel submissions are stored."
msgstr ""

#: includes/functions/AjaxFunctions.php:118
#: includes/functions/AjaxFunctions.php:276
msgid "Saved successfully . "
msgstr ""

#: includes/functions/AjaxFunctions.php:162
#: includes/functions/AjaxFunctions.php:178
#: includes/functions/AjaxFunctions.php:200
#: includes/functions/AjaxFunctions.php:222
#: includes/functions/AjaxFunctions.php:239
#: includes/functions/AjaxFunctions.php:263
msgid "Something went wrong ! Please try again"
msgstr ""

#: includes/functions/AjaxFunctions.php:197
msgid "Wheel trashed successfully."
msgstr ""

#: includes/functions/AjaxFunctions.php:197
msgid "Submission trashed successfully."
msgstr ""

#: includes/functions/AjaxFunctions.php:219
msgid "Wheel restored successfully."
msgstr ""

#: includes/functions/AjaxFunctions.php:219
msgid "Submission restored successfully."
msgstr ""

#: includes/functions/AjaxFunctions.php:235
msgid "Wheel deleted successfully."
msgstr ""

#: includes/functions/AjaxFunctions.php:235
msgid "Submission deleted successfully."
msgstr ""

#: includes/functions/AjaxFunctions.php:256
msgid "Copy of "
msgstr ""

#: includes/functions/AjaxFunctions.php:265
msgid "Wheel copied successfully . "
msgstr ""

#: includes/functions/AjaxFunctions.php:301
msgid "Please provide a valid email address."
msgstr ""

#: includes/functions/AjaxFunctions.php:305
msgid "Your Assessment report has been created!"
msgstr ""

#: includes/functions/AjaxFunctions.php:309
msgid "Dear {email},"
msgstr ""

#: includes/functions/AjaxFunctions.php:310
msgid ""
"Thank you for taking the Assessment. You can view your assessment "
"information via the link below."
msgstr ""

#: includes/functions/AjaxFunctions.php:311
msgid "My Wheel Report"
msgstr ""

#: includes/functions/AjaxFunctions.php:312
msgid "Thank you."
msgstr ""

#: includes/functions/AjaxFunctions.php:321
msgid "Email sent failed. Please try again later."
msgstr ""

#: includes/functions/AjaxFunctions.php:323
#: includes/functions/AjaxFunctions.php:330
msgid "Email sent successfully."
msgstr ""

#: includes/functions/AjaxFunctions.php:358
#: includes/functions/AjaxFunctions.php:375
msgid "Something went wrong! Please try again"
msgstr ""

#: includes/functions/AjaxFunctions.php:383
msgid "Report Saved Successfully."
msgstr ""

#: includes/functions/HelperFunctions.php:51
msgid "Is your life out of balance? Need a coach?"
msgstr ""

#: includes/functions/HelperFunctions.php:52
msgid ""
"I am a Certified Coach with experience of over 5 years. I can help you "
"transform your life and get it run smoothly."
msgstr ""

#: includes/functions/HelperFunctions.php:53
msgid "Schedule an Appointment"
msgstr ""

#: includes/functions/HelperFunctions.php:59
msgid "Services"
msgstr ""

#: templates/single-wheel-submissions.php:16
msgid "My Wheel of Life"
msgstr ""

#: templates/single-wheel-submissions.php:20
msgid "Wheel Submission No: #%d"
msgstr ""

#: templates/single-wheel-submissions.php:27
#: templates/single-wheel-submissions.php:98
#: templates/single-wheel-submissions.php:224
msgid "Assessment taken on: "
msgstr ""

#. Plugin URI of the plugin/theme
msgid "https://kraftplugins.com/wheel-of-life/"
msgstr ""

#. Description of the plugin/theme
msgid ""
"The Wheel of Life assessment tool helps clients quickly visualize every "
"vital aspect of their lives and helps them understand which area of their "
"life can be improved to bring in the perfect balance they need."
msgstr ""

#. Author of the plugin/theme
msgid "Kraft Plugins"
msgstr ""

#. Author URI of the plugin/theme
msgid "https://kraftplugins.com"
msgstr ""

#: includes/Wheel_Of_Life_PostTypes.php:62
msgctxt "post type general name"
msgid "Wheels"
msgstr ""

#: includes/Wheel_Of_Life_PostTypes.php:192
msgctxt "post type general name"
msgid "Submissions"
msgstr ""

#: includes/Wheel_Of_Life_PostTypes.php:63
msgctxt "post type singular name"
msgid "Wheel"
msgstr ""

#: includes/Wheel_Of_Life_PostTypes.php:193
msgctxt "post type singular name"
msgid "Submission"
msgstr ""

#: includes/Wheel_Of_Life_PostTypes.php:64
msgctxt "admin menu"
msgid "Wheels"
msgstr ""

#: includes/Wheel_Of_Life_PostTypes.php:194
msgctxt "admin menu"
msgid "Submissions"
msgstr ""

#: includes/Wheel_Of_Life_PostTypes.php:65
msgctxt "add new on admin bar"
msgid "Wheel"
msgstr ""

#: includes/Wheel_Of_Life_PostTypes.php:195
msgctxt "add new on admin bar"
msgid "Submission"
msgstr ""

#: includes/Wheel_Of_Life_PostTypes.php:66
msgctxt "wheel"
msgid "Add New"
msgstr ""

#: includes/Wheel_Of_Life_PostTypes.php:196
msgctxt "Submission"
msgid "Add New"
msgstr ""