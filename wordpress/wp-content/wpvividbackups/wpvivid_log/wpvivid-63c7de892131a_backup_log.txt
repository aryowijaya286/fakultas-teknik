Log created: 2023-01-18 11:56:57
Type: backup
[2023-01-18 11:57:09][notice]Start backing up.
[2023-01-18 11:57:09][notice]server info fcgi:Off max execution time: 900 wp version:6.1.1 php version:8.1.6 db version:10.4.24 php ini:safe_mode: memory_limit:256M memory_get_usage:62.78 MB memory_get_peak_usage:63.47 MB extensions:PDO enabled curl enabled zlib enabled   is_multisite:0
[2023-01-18 11:57:09][notice]Prepare to backup backup_db files.
[2023-01-18 11:57:09][notice]Start exporting database.
[2023-01-18 11:57:09][notice]max_allowed_packet less than 32M :1.00 MB
[2023-01-18 11:57:09][notice]Preparing to dump table wp_actionscheduler_actions
[2023-01-18 11:57:10][notice]Preparing to dump table wp_actionscheduler_claims
[2023-01-18 11:57:10][notice]Preparing to dump table wp_actionscheduler_groups
[2023-01-18 11:57:10][notice]Preparing to dump table wp_actionscheduler_logs
[2023-01-18 11:57:10][notice]Preparing to dump table wp_addonlibrary_addons
[2023-01-18 11:57:10][notice]Preparing to dump table wp_addonlibrary_categories
[2023-01-18 11:57:10][notice]Preparing to dump table wp_betterdocs_search_keyword
[2023-01-18 11:57:10][notice]Preparing to dump table wp_betterdocs_search_log
[2023-01-18 11:57:11][notice]Preparing to dump table wp_commentmeta
[2023-01-18 11:57:11][notice]Preparing to dump table wp_comments
[2023-01-18 11:57:11][notice]Preparing to dump table wp_e_events
[2023-01-18 11:57:11][notice]Preparing to dump table wp_links
[2023-01-18 11:57:11][notice]Preparing to dump table wp_nx_entries
[2023-01-18 11:57:11][notice]Preparing to dump table wp_nx_posts
[2023-01-18 11:57:11][notice]Preparing to dump table wp_nx_stats
[2023-01-18 11:57:12][notice]Preparing to dump table wp_options
[2023-01-18 11:57:13][notice]Preparing to dump table wp_postmeta
[2023-01-18 11:57:14][notice]Preparing to dump table wp_posts
[2023-01-18 11:57:14][notice]Preparing to dump table wp_reviewx_criterias
[2023-01-18 11:57:14][notice]Preparing to dump table wp_reviewx_import_history
[2023-01-18 11:57:15][notice]Preparing to dump table wp_reviewx_process_jobs
[2023-01-18 11:57:15][notice]Preparing to dump table wp_reviewx_reminder_email
[2023-01-18 11:57:15][notice]Preparing to dump table wp_termmeta
[2023-01-18 11:57:15][notice]Preparing to dump table wp_terms
[2023-01-18 11:57:15][notice]Preparing to dump table wp_term_relationships
[2023-01-18 11:57:15][notice]Preparing to dump table wp_term_taxonomy
[2023-01-18 11:57:16][notice]Preparing to dump table wp_usermeta
[2023-01-18 11:57:16][notice]Preparing to dump table wp_users
[2023-01-18 11:57:16][notice]Preparing to dump table wp_wpforms_tasks_meta
[2023-01-18 11:57:16][notice]Preparing to dump table wp_wpvivid_options
[2023-01-18 11:57:16][notice]Exporting database finished.
[2023-01-18 11:57:16][notice]Start compressing backup_db
[2023-01-18 11:57:16][notice]Prepare to zip files. file: localhost_wordpress_wpvivid-63c7de892131a_2023-01-18-11-56_backup_db.zip
[2023-01-18 11:57:17][notice]Adding zip files completed.localhost_wordpress_wpvivid-63c7de892131a_2023-01-18-11-56_backup_db.zip, filesize: 6.50 MB
[2023-01-18 11:57:17][notice]Compressing backup_db completed
[2023-01-18 11:57:17][notice]Backing up backup_db completed.
[2023-01-18 11:57:18][notice]Prepare to backup backup_themes files.
[2023-01-18 11:57:18][notice]File number: 1468
[2023-01-18 11:57:18][notice]Start compressing backup_themes
[2023-01-18 11:57:20][notice]Prepare to zip files. file: localhost_wordpress_wpvivid-63c7de892131a_2023-01-18-11-56_backup_themes.zip
[2023-01-18 11:58:30][notice]Adding zip files completed.localhost_wordpress_wpvivid-63c7de892131a_2023-01-18-11-56_backup_themes.zip, filesize: 49.52 MB
[2023-01-18 11:58:30][notice]Compressing backup_themes completed
[2023-01-18 11:58:30][notice]Backing up backup_themes completed.
[2023-01-18 11:58:37][notice]Prepare to backup backup_plugin files.
[2023-01-18 11:58:37][notice]File number: 11609
[2023-01-18 11:58:37][notice]Start compressing backup_plugin
[2023-01-18 11:59:03][notice]Prepare to zip files. file: localhost_wordpress_wpvivid-63c7de892131a_2023-01-18-11-56_backup_plugin.part001.zip
[2023-01-18 12:03:11][notice]Adding zip files completed.localhost_wordpress_wpvivid-63c7de892131a_2023-01-18-11-56_backup_plugin.part001.zip, filesize: 192.88 MB
[2023-01-18 12:03:11][notice]Prepare to zip files. file: localhost_wordpress_wpvivid-63c7de892131a_2023-01-18-11-56_backup_plugin.part002.zip
[2023-01-18 12:06:24][notice]Adding zip files completed.localhost_wordpress_wpvivid-63c7de892131a_2023-01-18-11-56_backup_plugin.part002.zip, filesize: 16.55 MB
[2023-01-18 12:06:24][notice]Compressing backup_plugin completed
[2023-01-18 12:06:24][notice]Backing up backup_plugin completed.
[2023-01-18 12:06:25][notice]Prepare to backup backup_uploads files.
[2023-01-18 12:06:25][notice]File number: 151
[2023-01-18 12:06:25][notice]Start compressing backup_uploads
[2023-01-18 12:06:25][notice]Prepare to zip files. file: localhost_wordpress_wpvivid-63c7de892131a_2023-01-18-11-56_backup_uploads.zip
[2023-01-18 12:06:29][notice]Adding zip files completed.localhost_wordpress_wpvivid-63c7de892131a_2023-01-18-11-56_backup_uploads.zip, filesize: 11.57 MB
[2023-01-18 12:06:29][notice]Compressing backup_uploads completed
[2023-01-18 12:06:29][notice]Backing up backup_uploads completed.
[2023-01-18 12:06:30][notice]Prepare to backup backup_content files.
[2023-01-18 12:06:30][notice]File number: 141
[2023-01-18 12:06:30][notice]Start compressing backup_content
[2023-01-18 12:06:30][notice]Prepare to zip files. file: localhost_wordpress_wpvivid-63c7de892131a_2023-01-18-11-56_backup_content.zip
[2023-01-18 12:06:34][notice]Adding zip files completed.localhost_wordpress_wpvivid-63c7de892131a_2023-01-18-11-56_backup_content.zip, filesize: 5.11 MB
[2023-01-18 12:06:34][notice]Compressing backup_content completed
[2023-01-18 12:06:34][notice]Backing up backup_content completed.
[2023-01-18 12:06:36][notice]Prepare to backup backup_core files.
[2023-01-18 12:06:36][notice]File number: 2525
[2023-01-18 12:06:36][notice]Start compressing backup_core
[2023-01-18 12:06:40][notice]Prepare to zip files. file: localhost_wordpress_wpvivid-63c7de892131a_2023-01-18-11-56_backup_core.zip
[2023-01-18 12:08:17][notice]Adding zip files completed.localhost_wordpress_wpvivid-63c7de892131a_2023-01-18-11-56_backup_core.zip, filesize: 49.83 MB
[2023-01-18 12:08:17][notice]Compressing backup_core completed
[2023-01-18 12:08:17][notice]Backing up backup_core completed.
[2023-01-18 12:08:18][notice]Prepare to backup backup_merge files.
[2023-01-18 12:08:18][notice]File number: 7
[2023-01-18 12:08:18][notice]Start compressing backup_merge
[2023-01-18 12:08:19][notice]Prepare to zip files. file: localhost_wordpress_wpvivid-63c7de892131a_2023-01-18-11-56_backup_all.part001.zip
[2023-01-18 12:08:33][notice]Adding zip files completed.localhost_wordpress_wpvivid-63c7de892131a_2023-01-18-11-56_backup_all.part001.zip, filesize: 67.93 MB
[2023-01-18 12:08:34][notice]Cleaned up file, filename: D:\APLIKASI PEMOGRAMAN\Xampp\htdocs\wordpress/wp-content\wpvividbackups\localhost_wordpress_wpvivid-63c7de892131a_2023-01-18-11-56_backup_plugin.part001.zip
[2023-01-18 12:08:34][notice]Prepare to zip files. file: localhost_wordpress_wpvivid-63c7de892131a_2023-01-18-11-56_backup_all.part002.zip
[2023-01-18 12:08:43][notice]Adding zip files completed.localhost_wordpress_wpvivid-63c7de892131a_2023-01-18-11-56_backup_all.part002.zip, filesize: 63.37 MB
[2023-01-18 12:08:43][notice]Cleaned up file, filename: D:\APLIKASI PEMOGRAMAN\Xampp\htdocs\wordpress/wp-content\wpvividbackups\localhost_wordpress_wpvivid-63c7de892131a_2023-01-18-11-56_backup_core.zip
[2023-01-18 12:08:43][notice]Cleaned up file, filename: D:\APLIKASI PEMOGRAMAN\Xampp\htdocs\wordpress/wp-content\wpvividbackups\localhost_wordpress_wpvivid-63c7de892131a_2023-01-18-11-56_backup_themes.zip
[2023-01-18 12:08:43][notice]Cleaned up file, filename: D:\APLIKASI PEMOGRAMAN\Xampp\htdocs\wordpress/wp-content\wpvividbackups\localhost_wordpress_wpvivid-63c7de892131a_2023-01-18-11-56_backup_plugin.part002.zip
[2023-01-18 12:08:43][notice]Cleaned up file, filename: D:\APLIKASI PEMOGRAMAN\Xampp\htdocs\wordpress/wp-content\wpvividbackups\localhost_wordpress_wpvivid-63c7de892131a_2023-01-18-11-56_backup_uploads.zip
[2023-01-18 12:08:43][notice]Cleaned up file, filename: D:\APLIKASI PEMOGRAMAN\Xampp\htdocs\wordpress/wp-content\wpvividbackups\localhost_wordpress_wpvivid-63c7de892131a_2023-01-18-11-56_backup_db.zip
[2023-01-18 12:08:43][notice]Cleaned up file, filename: D:\APLIKASI PEMOGRAMAN\Xampp\htdocs\wordpress/wp-content\wpvividbackups\localhost_wordpress_wpvivid-63c7de892131a_2023-01-18-11-56_backup_content.zip
[2023-01-18 12:08:43][notice]Compressing backup_merge completed
[2023-01-18 12:08:43][notice]Backing up backup_merge completed.
[2023-01-18 12:08:43][notice]Backup completed.
[2023-01-18 12:08:43][notice]Backup succeeded.
